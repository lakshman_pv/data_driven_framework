package com.automation.commonFunctionalities;

import java.io.File;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

@Component
public class ExtendReportClass extends BaseClass {
	
	public ExtentReports extentReport;
	public ExtentTest extentTest;
	
	/**
	 * @author Wisefinch Menaka
	 * @see to create extent report
	 * 
	 * @param fileName --> Extent report html file will be created with the name
	 *                 passed here
	 * @return --> created extent report will be returned
	 */
	public ExtentReports initExtentReport(String fileName) {
		try {
			//--> System.out.println("********** workingDir : " + workingDir);
			pathToStoreFiles = workingDir + "\\TestReports\\" + fileName + "\\";
			extentReport = new ExtentReports(pathToStoreFiles + fileName + ".html", true);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During init report " + e);
		}
		return extentReport;
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To create instance of extent test
	 * 
	 * @param extentReports --> Current instance of {@link ExtentReports}
	 * @param testName      --> test name
	 * @return --> new extent test
	 */
	public ExtentTest initExtentTest(ExtentReports extentReports, String testName) {
		try {
			//--> System.out.println("********** workingDir : " + workingDir);
			extentTest = extentReports.startTest(testName);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During init report " + e);
		}
		return extentTest;
	}

	/**
	 * @author Wisefinch Menaka
	 * @see to perfrom end test and flush all current details to report
	 * 
	 * @param extentReports --> Current instance of {@link ExtentReports}
	 * @param extentTest    --> Current instance of {@link ExtentTest}
	 */
	public void endReport(ExtentReports extentReports, ExtentTest extentTest, String validationPoint) {
		try {
			extentReports.endTest(extentTest);
			extentReports.flush();
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During init report " + e);
		}
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status pass
	 * 
	 * @param webDriver       --> current instance of {@link WebDriver}
	 * @param extentTest      --> current istance of {@link ExtentTest}
	 * @param passLogMessage  --> Message to log in extentTest
	 * @param validationPoint --> If the value is Y Screen shot will be attached to
	 *                        the report.For N there won't be any screen shot
	 *                        attached.
	 * @throws IOException
	 */
	public void reportPass(WebDriver webDriver, ExtentTest extentTest, String validationPoint, String passLogMessage)
			throws IOException {
		try {
			webDriver.toString();

			if (validationPoint.equalsIgnoreCase("Y")) {
				//--> System.out.println("********** webDriver.toString();" + webDriver.toString());
				if (webDriver.toString().contains("AndroidDriver") || webDriver.toString().contains("AppiumDriver")
						|| webDriver.toString().contains("IOSDriver")) {
					// System.out.println("********** waiting to take screen shot");
					Thread.sleep(3000);
				}
				extentTest.log(LogStatus.PASS, passLogMessage + extentTest.addScreenCapture(capture(webDriver)));
			} else {
				extentTest.log(LogStatus.PASS, passLogMessage);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Pass Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status Fail
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param failLog    --> Message to log in extentTest
	 */
	public void reportFail(WebDriver webDriver, ExtentTest extentTest, String failLog) {
		try {

			if (webDriver.toString().contains("AndroidDriver") || webDriver.toString().contains("AppiumDriver")
					|| webDriver.toString().contains("IOSDriver")) {
				// System.out.println("********** waiting to take screen shot");
				Thread.sleep(3000);
			}
			extentTest.log(LogStatus.FAIL, failLog + extentTest.addScreenCapture(capture(webDriver)));
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Fail Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status skip
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param skipLog    --> Message to log in extentTest
	 */
	public void reportSkip(WebDriver webDriver, ExtentTest extentTest, String skipLog) {
		try {
			extentTest.log(LogStatus.SKIP, skipLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Skip Log " + e);
		}
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status Info
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param infoLog    --> Message to log in extentTest
	 */
	public void reportInfo(WebDriver webDriver, ExtentTest extentTest, String infoLog) {
		try {
			extentTest.log(LogStatus.INFO, infoLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Info Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status Info
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param infoLog    --> Message to log in extentTest
	 */
	public void apiReportInfo(ExtentTest extentTest, String infoLog) {
		try {
			extentTest.log(LogStatus.INFO, infoLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Info Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status error
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param errorLog   --> Message to log in extentTest
	 */
	public void reportError(WebDriver webDriver, ExtentTest extentTest, String errorLog) {
		try {
			extentTest.log(LogStatus.ERROR, errorLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Error Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status fatal
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param fatalLog   --> Message to log in extentTest
	 */
	public void reportFatal(WebDriver webDriver, ExtentTest extentTest, String fatalLog) {
		try {
			extentTest.log(LogStatus.FATAL, fatalLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During fatal Log " + e);
		}

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status unknown
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param unknownLog --> Message to log in extentTest
	 */
	public void reportUnknown(WebDriver webDriver, ExtentTest extentTest, String unknownLog) {
		try {
			extentTest.log(LogStatus.UNKNOWN, unknownLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During unknown Log " + e);
		}
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add log to the extentTest with the status warning
	 * 
	 * @param webDriver  --> current instance of {@link WebDriver}
	 * @param extentTest --> current instance of {@link ExtentTest}
	 * @param warningLog --> Message to log in extentTest
	 */
	public void reportWarning(WebDriver webDriver, ExtentTest extentTest, String warningLog) {
		try {
			extentTest.log(LogStatus.WARNING, warningLog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During warning Log " + e);
		}
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To take screen shot
	 * 
	 * @param driver --> current instance of {@link WebDriver}
	 * @return --> File name
	 * @throws IOException
	 */
	public String capture(WebDriver driver) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		//--> System.out.println("********** pathToStoreFiles : " + pathToStoreFiles);
		String fileName = "ScreenShot" + System.currentTimeMillis() + ".png";
		File Dest = new File(pathToStoreFiles + fileName);
		String screenShotFilepath = Dest.getName();
		//--> System.out.println("********** screenShotFilepath : " + screenShotFilepath);
		FileUtils.copyFile(scrFile, Dest);
		return fileName;
	}
	

	/**
	 * @author Wisefinch Menaka
	 * @see init extent test for a extent report
	 */
	public void initExtentTest(String scenarioName) {
		try {
			// extendReportClass = new ExtendReport();
			/*
			 * String testCaseNameForReport = scenarioName + "_" +
			 * commonReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss");
			 */
			// System.out.println("*********** testCaseNameForReport : " +
			// testCaseNameForReport);
			// extentReport = extendReportClass.initExtentReport(testCaseNameForReport);
			extentTest = initExtentTest(extentReport, scenarioName);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during init extend test " + e);
			reportFail(webDriver, extentTest, "Exception during init extend test " + e);
		}
	}
}
