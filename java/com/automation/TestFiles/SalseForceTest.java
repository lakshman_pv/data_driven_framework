package com.automation.TestFiles;

import com.automation.commonFunctionalities.CommenReusableMethods;
import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.automation.letsMakeItSimplePOM.InitPage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SalseForceTest extends BaseClass {

	public String launchURL, browserToLaunch;

	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();
	ExtendReportClass extendReportClass = new ExtendReportClass();
	CommenReusableMethods commonReusableMethods = new CommenReusableMethods();
	ExtendReportClass extentReportClass = new ExtendReportClass();
	
	@BeforeSuite
	public void before_all() throws ThrowNewException, FileNotFoundException {
		System.out.println("********** Before Suit called");
		commonReusableMethods.readPropertyFile();

		launchURL = properties.getProperty("Salesforce");
		browserToLaunch = properties.getProperty("Browser");

		extentReport = extentReportClass
				.initExtentReport("ExecutionReport" + commonReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss"));
	}

	@BeforeMethod
	public void initReport() throws ThrowNewException {
		System.out.println("********** Before test");
		webDriver = seleniumReusableMethods.initWebDriver(browserToLaunch, extentReport, extentTest, "Y");
		InitPage initPage = new InitPage(webDriver);
		System.out.println("********** Webdriver launched");
	}

	@AfterMethod
	public void closeBrowser() {
		webDriver.quit();
	}

	@AfterSuite
	public void closeExtendReport() {
		extentReportClass.endReport(extentReport, extentTest, pathToStoreFiles);
		System.out.println("********** After test");
	}

	
	@Test
	public void accountCreation() throws ThrowNewException, IOException {
		try {
			testCaseNumber = "TC001";
			extentTest = extentReportClass.initExtentTest(extentReport, "Create New Account");
			extentReportClass.reportInfo(webDriver, extentTest, "Test Case Name : Create New Account And Verify.");
			seleniumReusableMethods.launchWebDriver(webDriver, extentReport, extentTest, "Y", launchURL);
			salesForceLoginPage.loginToSalseForceWebPage(extentReport, extentTest, "Y");
			salesforceHomepage.launchSalesForceApp(extentReport, extentTest, "Y", "Accounts");
			SalesForceAccountsPage.createNewAccount(extentReport, extentTest, "Y");
		} catch (Exception e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to execute accountCreation test. " + e);
			e.printStackTrace();
		}
	}
}
