package com.automation.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class EntitlementPage_OrangeHRM extends BaseClass {

	private WebDriver webDriver;

	public EntitlementPage_OrangeHRM(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);

	}

	@FindBy(xpath = ".//*[@name='entitlements[employee][empName]']")
	public WebElement employeeName;

	@FindBy(xpath = ".//*[@id='entitlements_filters_bulk_assign']")
	public WebElement addToMultipleEmployee;

	@FindBy(xpath = ".//*[@id='entitlements_leave_type']")
	public WebElement leaveType;

	@FindBy(xpath = ".//*[@id='period']")
	public WebElement leavePeriod;

	@FindBy(xpath = ".//*[@name='entitlements[entitlement]']")
	public WebElement entitlement;

	@FindBy(xpath = ".//*[@id='btnSave']")
	public WebElement entitilementSave;


}
