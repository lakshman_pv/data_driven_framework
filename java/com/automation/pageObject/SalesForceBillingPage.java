package com.automation.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class SalesForceBillingPage extends BaseClass {

    private Object webDriver;

    public SalesForceBillingPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = ".//*[@title='New']")
    public WebElement newButtonBilling;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Proprietary_Billing_Number__c']")
    public WebElement billingNumber;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Credit_Memo_Applied_Amount__c']")
    public WebElement creditMemoAppliedAmount;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[4]")
    public WebElement accountingPeriod;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[2]")
    public WebElement postingStatus;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[3]")
    public WebElement ledger;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[5]")
    public WebElement currency;

    @FindBy(xpath = ".//*[@name='Currency_Conversion_Rate__c']")
    public WebElement currencyConversionRate;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Discount__c']")
    public WebElement discount;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[6]")
    public WebElement customer;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='PO_Number__c']")
    public WebElement poNumber;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[7]")
    public WebElement opportunity;

    @FindBy(xpath = "(.//*[@class='actionBody']//textarea)[1]")
    public WebElement billingComment;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name=\"Billing_Date__c\"]")
    public WebElement billingDate;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_Cycle_Start_Date__c']")
    public WebElement billingCycleStartDate;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_Terms_Name__c']")
    public WebElement billingTermsName;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_Cycle_End_Date__c']")
    public WebElement billingCycleEndDate;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Discount_Due_Date__c']")
    public WebElement discountDueDate;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Due_Date__c']")
    public WebElement dueDate;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[8]")
    public WebElement billingContact;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[9]")
    public WebElement shippingContact;

    @FindBy(xpath = "(.//*[@class='actionBody']//textarea)[2]")
    public WebElement billingStreet;

    @FindBy(xpath = "(.//*[@class='actionBody']//textarea)[3]")
    public WebElement shippingStreet;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_City__c']")
    public WebElement billingCity;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Shipping_City__c']")
    public WebElement shippingCity;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_State__c']")
    public WebElement billingState;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Shipping_State__c']")
    public WebElement shippingState;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_Country__c']")
    public WebElement billingCountry;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Shipping_Country__c']")
    public WebElement shippingCountry;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Billing_Postal_Code__c']")
    public WebElement billingPostalCode;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[10]")
    public WebElement billingFormat;

    @FindBy(xpath = "(.//*[@class='actionBody']//input[@role='combobox'])[11]")
    public WebElement pDFEmailFormat;

    @FindBy(xpath = ".//*[@class='actionBody']//input[@name='Payment_Lin__c']")
    public WebElement paymentLink;

    @FindBy(xpath = ".//*[@class='actionBody']//button[@name='SaveEdit']")
    public WebElement saveAndEdit;

    @FindBy(xpath = ".//*[@slot='primaryField']/lightning-formatted-text")
    public WebElement newBillingName;

    @FindBy(xpath = ".//*[@data-label='Related']//*[contains(text(),'Related')]")
    public WebElement related;

    @FindBy(xpath = ".//*[@name='New']")
    public WebElement newBillingLine;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Rate__c']")
    public WebElement unitPrice;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='Tax_Amount2__c']")
    public WebElement taxAmount;

    @FindBy(xpath = ".//*[@class='actionBody']//*[@name='SaveEdit']")
    public WebElement billingLineSave;

    @FindBy(xpath = ".//*[@data-aura-class='forceOutputLookup' and contains(text(),'BL-')]")
    public WebElement firstBillingLine;

    String selectValueForDropDown = ".//*[@class='actionBody']//*[contains(text(),'%s')]";

    String selectLightingFormatText = "//lightning-base-combobox-formatted-text[@title='%s']";
}
