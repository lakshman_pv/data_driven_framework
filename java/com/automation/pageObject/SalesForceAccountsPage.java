package com.automation.pageObject;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

@Component
public class SalesForceAccountsPage extends BaseClass {

	private WebDriver webDriver;
	ExtendReportClass extendReportClass = new ExtendReportClass();
	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();

	public SalesForceAccountsPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(xpath = ".//a/*[contains(text(),'New')]")
	public WebElement newButton;

	@FindBy(xpath = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement dropDownIcon;

	@FindBy(xpath = "//a[@name='Edit']")
	public WebElement editAccount;

	@FindBy(xpath = "//input[@placeholder='Search this list...']")
	public WebElement searchField;

	@FindBy(xpath = "//table//tbody//tr[1]")
	public WebElement firstAccount;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Name']")
	public WebElement accountName;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[1]")
	public WebElement rating;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[2]")
	public WebElement parentAccount;

	@FindBy(xpath = "//button[@title='Clear Selection']")
	public WebElement clearParentAccount;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Phone']")
	public WebElement phone;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Fax']")
	public WebElement fax;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='AccountNumber']")
	public WebElement accountNumber;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Website']")
	public WebElement website;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Site']")
	public WebElement accountSite;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='TickerSymbol']")
	public WebElement tickerSymbol;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[3]")
	public WebElement type;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[4]")
	public WebElement owner;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[5]")
	public WebElement industry;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='NumberOfEmployees']")
	public WebElement employees;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='AnnualRevenue']")
	public WebElement annualRevenue;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Sic']")
	public WebElement sicCode;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='street'])[1]")
	public WebElement billingAddressStreet;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='street'])[2]")
	public WebElement shippingAddressStreet;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='city'])[1]")
	public WebElement billingAddressCity;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='city'])[2]")
	public WebElement shippingAddressCity;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='province'])[1]")
	public WebElement billingState;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='province'])[2]")
	public WebElement shippingState;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='postalCode'])[1]")
	public WebElement billingZip;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='postalCode'])[2]")
	public WebElement shippingZip;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='country'])[1]")
	public WebElement billingCountry;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@name='country'])[2]")
	public WebElement shippingCountry;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[6]")
	public WebElement customerPriority;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[7]")
	public WebElement sla;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='SLAExpirationDate__c']")
	public WebElement slaExpirationDate;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='SLASerialNumber__c']")
	public WebElement slaSerialNumber;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='NumberofLocations__c']")
	public WebElement numberOfLocations;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[8]")
	public WebElement upsellOpurtunity;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[9]")
	public WebElement active;

	@FindBy(xpath = ".//*[@class='actionBody']//force-record-layout-section[4]//textarea")
	public WebElement description;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='SaveEdit']")
	public WebElement save;

	public String searchAccountTable = "//tbody/tr/th//a[@title='%s']";

	public String selectValueFromDropDown = ".//*[@class='actionBody']//*[contains(text(),'%s')]";

	public String selectLightningFormatText = "//lightning-base-combobox-formatted-text[@title='%s']";

	public String selectTypeValue = ".//*[@class='actionBody']//span[contains(text(),'%s')]";

	public String newAccountCheck = ".//*[@name='primaryField']//*[@data-aura-class='uiOutputText' and contains(text(),'%s')]";

	public void createNewAccount(ExtentReports extentReport, ExtentTest extentTest, String validationPoint)
			throws InterruptedException {

		String accountNameToenter = properties.getProperty(testCaseNumber + "_AccountName");
		String rate = properties.getProperty(testCaseNumber + "_Rate");
		String parentAccount = properties.getProperty(testCaseNumber + "_ParentAccount");
		String phone = properties.getProperty(testCaseNumber + "_Phone");
		String type = properties.getProperty(testCaseNumber + "_Type");
		String ownerShipType = properties.getProperty(testCaseNumber + "_OwnerShipType");
		String billingAddressStreet = properties.getProperty(testCaseNumber + "_BillingAddressStreet");
		String billingAddressCity = properties.getProperty(testCaseNumber + "_BillingAddressCity");
		String billingAddressState = properties.getProperty(testCaseNumber + "_BillingAddressState");
		String billingAddressZip = properties.getProperty(testCaseNumber + "_BillingAddressZip");
		String billingAddressCountry = properties.getProperty(testCaseNumber + "_BillingAddressCountry");

		try {
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, validationPoint, "New Button",
					newButton, "isDisplayed");
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "New button",
					newButton);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "New Account Name",
					accountName, accountNameToenter);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Rate", rating);

			String selectRate = selectValueFromDropDown.replaceAll("%s", rate);
			WebElement identifyElementToSelectRate = seleniumReusableMethods.findElement(webDriver, "xpath",
					selectRate);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Rate",
					identifyElementToSelectRate);

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Parent Account",
					this.parentAccount);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "Parent Account",
					this.parentAccount, parentAccount);

			String selectParentAccount = selectLightningFormatText.replaceAll("%s", parentAccount);
			WebElement identifyElementToSelectParentAccount = seleniumReusableMethods.findElement(webDriver, "xpath",
					selectParentAccount);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Parent Account",
					identifyElementToSelectParentAccount);

			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "Phone Number",
					this.phone, phone);
			seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, validationPoint,
					"Account Type", this.type);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Type", this.type);

			String selectType = selectTypeValue.replaceAll("%s", type);
			WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
					selectType);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Account Type",
					identifyElementToSelectType);

			seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, validationPoint,
					"Account Ownership", this.owner);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Account Ownershio",
					this.owner);

			String selectOwner = selectTypeValue.replaceAll("%s", ownerShipType);
			WebElement identifyElementToSelectOwnerShip = seleniumReusableMethods.findElement(webDriver, "xpath",
					selectOwner);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Account Ownership",
					identifyElementToSelectOwnerShip);

			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint,
					"Billing Address Street", this.billingAddressStreet, billingAddressStreet);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint,
					"Billing Address City", this.billingAddressCity, billingAddressCity);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint,
					"Billing Address State", this.billingState, billingAddressState);

			seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, validationPoint,
					"Billing Zip", this.billingZip);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "Billing Zip",
					this.billingZip, billingAddressZip);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "Billing Country",
					this.billingCountry, billingAddressCountry);

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Save", save);

			String verifyNewAccount = newAccountCheck.replaceAll("%s", runTimeTestDatas.get("New Account Name"));
			WebElement identifyElementToCheckNewAccount = seleniumReusableMethods.findElement(webDriver, "xpath",
					verifyNewAccount);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, validationPoint, "New Account Check",
					identifyElementToCheckNewAccount, "isDisplayed");

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform account creation. " + e);
			e.printStackTrace();
		}

	}

	public boolean searchAccount(ExtentReports extentReport, ExtentTest extentTest, String AccountName)
			throws IOException, ThrowNewException {
		boolean stat = true;
		try {
			if (AccountName != null) {

				System.out.println(stat);
				seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "Account Search", searchField,
						"isDisplayed");
				seleniumReusableMethods.sendKeyAndEnter(webDriver, extentReport, extentTest, "Y", "Account Search",
						searchField, AccountName);
				String searchAccount = searchAccountTable.replaceAll("%s", AccountName);
				System.out.println("xpath : " + searchAccount);
				seleniumReusableMethods.verifyElement(webDriver, extentReport, extentTest, "Y", "searchAccount",
						searchAccount, "isDisplayed");
				if (methodexecutionStat == true) {
					System.out.println("Account existing");
				} else {
					System.out.println("Account to be created");
					stat = false;
				}

			}

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform account search. " + e);
			e.printStackTrace();
			return stat = false;
		}
		return stat;

	}

	public boolean editAccount(ExtentReports extentReport, ExtentTest extentTest, String AccountName, String Rating,
			String Phone, String Parent, String Fax, String AccountNumber, String Website, String AccountSite,
			String TickerSymbol, String Type, String Ownership, String Industry, String Employees, String AnnualRevenue,
			String SICcode, String BillingStreet, String BillingCity, String ShippingStreet, String BillingSP,
			String ShippingCity, String ShippingSP, String BillingZIP, String BillingCountry, String ShippingZIP,
			String ShippingCountry, String CustomerPriority, String SLA, String SLAExpirationDate, String SLASerialNo,
			String NoOfLocations, String UpsellOppurtunity, String Active, String Description)
			throws IOException, ThrowNewException, InterruptedException {
		boolean stat = true;
		try {
			seleniumReusableMethods.refresh(webDriver, extentReport, extentTest, "N");
			String searchAccount = searchAccountTable.replaceAll("%s", AccountName);
			WebElement clickAccount = seleniumReusableMethods.findElement(webDriver, "xpath", searchAccount);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Account", clickAccount);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "dropDownTrigger", dropDownIcon,
					"isDisplayed");
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "dropDownTrigger", dropDownIcon);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Edit Account", editAccount);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "Account Name", accountName,
					"isDisplayed");
			if (Rating == null || Rating == "" || Rating == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Rating", rating);
				String selectRating = selectValueFromDropDown.replaceAll("%s", Rating);
				WebElement selectRatingElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectRating);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "selectRating",
						selectRatingElement);

			}

			if (Phone == null || Phone == "" || Phone == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Phone", phone, Phone);
			}

			if (Parent == null || Parent == "" || Parent == " ") {

				System.out.println("error value");
			} else {

				if (seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "clear ParentAccount",
						clearParentAccount, "isDisplayed")) {

					seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "clear ParentAccount",
							clearParentAccount);
				}

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Parent Account",
						parentAccount, Parent);
				String selectParent = selectLightningFormatText.replaceAll("%s", Parent);
				WebElement selectParentElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectParent);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "selectParent",
						selectParentElement);
			}

			if (Fax == null || Fax == "" || Fax == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Fax", fax, Fax);
			}

			if (AccountNumber == null || AccountNumber == "" || AccountNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Account Number",
						accountNumber, AccountNumber);
			}

			if (Website == null || Website == "" || Website == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Website", website, Website);
			}

			if (AccountSite == null || AccountSite == "" || AccountSite == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Account Site", accountSite,
						AccountSite);
			}

			if (TickerSymbol == null || TickerSymbol == "" || TickerSymbol == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "TickerSymbol", tickerSymbol,
						TickerSymbol);
			}

			if (Type == null || Type == "" || Type == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Type", type);

				String selectType = selectTypeValue.replaceAll("%s", Type);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Account Type",
						identifyElementToSelectType);
			}

			if (Ownership == null || Ownership == "" || Ownership == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Ownership", owner);

				String selectType = selectTypeValue.replaceAll("%s", Ownership);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Ownership select",
						identifyElementToSelectType);
			}

			if (Industry == null || Industry == "" || Industry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Idustry",
						industry);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Industry", industry);

				String selectType = selectTypeValue.replaceAll("%s", Industry);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Industry select",
						identifyElementToSelectType);
			}

			if (Employees == null || Employees == "" || Employees == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Employees", employees,
						Employees);
			}

			if (AnnualRevenue == null || AnnualRevenue == "" || AnnualRevenue == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "AnnualRevenue",
						annualRevenue, AnnualRevenue);
			}

			if (SICcode == null || SICcode == "" || SICcode == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SICcode", sicCode, SICcode);
			}

			if (BillingStreet == null || BillingStreet == "" || BillingStreet == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingStreet",
						billingAddressStreet, BillingStreet);
			}

			if (ShippingStreet == null || ShippingStreet == "" || ShippingStreet == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingStreet",
						shippingAddressStreet, ShippingStreet);
			}

			if (BillingCity == null || BillingCity == "" || BillingCity == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingCity", billingAddressCity,
						BillingCity);
			}

			if (ShippingCity == null || ShippingCity == "" || ShippingCity == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingCity", shippingAddressCity,
						ShippingCity);
			}

			if (BillingSP == null || BillingSP == "" || BillingSP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingSP", billingState,
						BillingSP);
			}

			if (ShippingSP == null || ShippingSP == "" || ShippingSP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingSP", shippingState,
						ShippingSP);
			}

			if (BillingZIP == null || BillingZIP == "" || BillingZIP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingZIP", billingZip,
						BillingZIP);
			}

			if (ShippingZIP == null || ShippingZIP == "" || ShippingZIP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingZIP", shippingZip,
						ShippingZIP);
			}

			if (BillingCountry == null || BillingCountry == "" || BillingCountry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingCountry", billingCountry,
						BillingCountry);
			}

			if (ShippingCountry == null || ShippingCountry == "" || ShippingCountry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingCountry", shippingCountry,
						ShippingCountry);
			}
			
			if (CustomerPriority == null || CustomerPriority == "" || CustomerPriority == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "CustomerPriority",
						customerPriority);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "CustomerPriority", customerPriority);

				String selectType = selectTypeValue.replaceAll("%s", CustomerPriority);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "CustomerPriority select",
						identifyElementToSelectType);
			}
			
			if (SLA == null || SLA == "" || SLA == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "SLA", sla);

				String selectType = selectTypeValue.replaceAll("%s", SLA);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "SLA select",
						identifyElementToSelectType);
			}
			
			if (SLAExpirationDate == null || SLAExpirationDate == "" || SLAExpirationDate == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SLAExpirationDate", slaExpirationDate,
						SLAExpirationDate);
			}
			
			if (SLASerialNo == null || SLASerialNo == "" || SLASerialNo == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SLASerialNo", slaSerialNumber,
						SLASerialNo);
			}
			
			if (NoOfLocations == null || NoOfLocations == "" || NoOfLocations == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "NoOfLocations", numberOfLocations,
						NoOfLocations);
			}
			
			if (UpsellOppurtunity == null || UpsellOppurtunity == "" || UpsellOppurtunity == " ") {

				System.out.println("error value");
			} else {
				
				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "UpsellOppurtunity",
						upsellOpurtunity);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "UpsellOppurtunity", upsellOpurtunity);

				String selectType = selectTypeValue.replaceAll("%s", UpsellOppurtunity);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "UpsellOppurtunity select",
						identifyElementToSelectType);
			}
			
			if (Active == null || Active == "" || Active == " ") {

				System.out.println("error value");
			} else {
				
				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Active",
						active);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Active", active);

				String selectType = selectTypeValue.replaceAll("%s", Active);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Active select",
						identifyElementToSelectType);
			}
			
			if (Description == null || Description == "" || Description == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Description", description,
						Description);
			}

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Save", save);

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform account search. " + e);
			e.printStackTrace();
			return stat = false;
		}
		return stat;

	}

	public boolean createAccount(ExtentReports extentReport, ExtentTest extentTest, String AccountName, String Rating,
			String Phone, String Parent, String Fax, String AccountNumber, String Website, String AccountSite,
			String TickerSymbol, String Type, String Ownership, String Industry, String Employees, String AnnualRevenue,
			String SICcode, String BillingStreet, String BillingCity, String ShippingStreet, String BillingSP,
			String ShippingCity, String ShippingSP, String BillingZIP, String BillingCountry, String ShippingZIP,
			String ShippingCountry, String CustomerPriority, String SLA, String SLAExpirationDate, String SLASerialNo,
			String NoOfLocations, String UpsellOppurtunity, String Active, String Description)
			throws InterruptedException {
		boolean methodStat = true;
		try {

			seleniumReusableMethods.refresh(webDriver, extentReport, extentTest, "N");
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "New Button", newButton,
					"isDisplayed");
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "New button", newButton);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "New Account Name", accountName,
					AccountName);

			if (Rating == null || Rating == "" || Rating == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Rate", rating);

				String selectRate = selectValueFromDropDown.replaceAll("%s", Rating);
				WebElement identifyElementToSelectRate = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectRate);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Rate",
						identifyElementToSelectRate);
			}
			
			if (Phone == null || Phone == "" || Phone == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Phone", phone, Phone);
			}
			
			if (Parent == null || Parent == "" || Parent == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Parent Account",
						parentAccount);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Parent Account",
						parentAccount, Parent);

				String selectParentAccount = selectLightningFormatText.replaceAll("%s", Parent);
				WebElement identifyElementToSelectParentAccount = seleniumReusableMethods.findElement(webDriver,
						"xpath", selectParentAccount);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Parent Account",
						identifyElementToSelectParentAccount);
			}

			if (Fax == null || Fax == "" || Fax == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Fax", fax, Fax);
			}

			if (AccountNumber == null || AccountNumber == "" || AccountNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Account Number",
						accountNumber, AccountNumber);
			}

			if (Website == null || Website == "" || Website == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Website", website, Website);
			}

			if (AccountSite == null || AccountSite == "" || AccountSite == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Account Site", accountSite,
						AccountSite);
			}

			if (TickerSymbol == null || TickerSymbol == "" || TickerSymbol == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "TickerSymbol", tickerSymbol,
						TickerSymbol);
			}

			if (Type == null || Type == "" || Type == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Account Type",
						type);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Type", type);

				String selectType = selectTypeValue.replaceAll("%s", Type);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Account Type",
						identifyElementToSelectType);
			}

			if (Ownership == null || Ownership == "" || Ownership == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Ownership", owner);

				String selectType = selectTypeValue.replaceAll("%s", Ownership);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Ownership select",
						identifyElementToSelectType);
			}

			if (Industry == null || Industry == "" || Industry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Idustry",
						industry);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Industry", industry);

				String selectType = selectTypeValue.replaceAll("%s", Industry);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Industry select",
						identifyElementToSelectType);
			}

			if (Employees == null || Employees == "" || Employees == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Employees", employees,
						Employees);
			}

			if (AnnualRevenue == null || AnnualRevenue == "" || AnnualRevenue == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "AnnualRevenue",
						annualRevenue, AnnualRevenue);
			}

			if (SICcode == null || SICcode == "" || SICcode == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SICcode", sicCode, SICcode);
			}

			if (BillingStreet == null || BillingStreet == "" || BillingStreet == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingStreet",
						billingAddressStreet, BillingStreet);
			}

			if (ShippingStreet == null || ShippingStreet == "" || ShippingStreet == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingStreet",
						shippingAddressStreet, ShippingStreet);
			}

			if (BillingCity == null || BillingCity == "" || BillingCity == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingCity", billingAddressCity,
						BillingCity);
			}

			if (ShippingCity == null || ShippingCity == "" || ShippingCity == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingCity", shippingAddressCity,
						ShippingCity);
			}

			if (BillingSP == null || BillingSP == "" || BillingSP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingSP", billingState,
						BillingSP);
			}

			if (ShippingSP == null || ShippingSP == "" || ShippingSP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingSP", shippingState,
						ShippingSP);
			}

			if (BillingZIP == null || BillingZIP == "" || BillingZIP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingZIP", billingZip,
						BillingZIP);
			}

			if (ShippingZIP == null || ShippingZIP == "" || ShippingZIP == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingZIP", shippingZip,
						ShippingZIP);
			}

			if (BillingCountry == null || BillingCountry == "" || BillingCountry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "BillingCountry", billingCountry,
						BillingCountry);
			}

			if (ShippingCountry == null || ShippingCountry == "" || ShippingCountry == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "ShippingCountry", shippingCountry,
						ShippingCountry);
			}
			
			if (CustomerPriority == null || CustomerPriority == "" || CustomerPriority == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "CustomerPriority",
						customerPriority);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "CustomerPriority", customerPriority);

				String selectType = selectTypeValue.replaceAll("%s", CustomerPriority);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "CustomerPriority select",
						identifyElementToSelectType);
			}
			
			if (SLA == null || SLA == "" || SLA == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "SLA", sla);

				String selectType = selectTypeValue.replaceAll("%s", SLA);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "SLA select",
						identifyElementToSelectType);
			}
			
			if (SLAExpirationDate == null || SLAExpirationDate == "" || SLAExpirationDate == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SLAExpirationDate", slaExpirationDate,
						SLAExpirationDate);
			}
			
			if (SLASerialNo == null || SLASerialNo == "" || SLASerialNo == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "SLASerialNo", slaSerialNumber,
						SLASerialNo);
			}
			
			if (NoOfLocations == null || NoOfLocations == "" || NoOfLocations == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "NoOfLocations", numberOfLocations,
						NoOfLocations);
			}
			
			if (UpsellOppurtunity == null || UpsellOppurtunity == "" || UpsellOppurtunity == " ") {

				System.out.println("error value");
			} else {
				
				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "UpsellOppurtunity",
						upsellOpurtunity);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "UpsellOppurtunity", upsellOpurtunity);

				String selectType = selectTypeValue.replaceAll("%s", UpsellOppurtunity);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "UpsellOppurtunity select",
						identifyElementToSelectType);
			}
			
			if (Active == null || Active == "" || Active == " ") {

				System.out.println("error value");
			} else {
				
				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Active",
						active);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Active", active);

				String selectType = selectTypeValue.replaceAll("%s", Active);
				WebElement identifyElementToSelectType = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Active select",
						identifyElementToSelectType);
			}
			
			if (Description == null || Description == "" || Description == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Description", description,
						Description);
			}

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Save", save);

			String verifyNewAccount = newAccountCheck.replaceAll("%s", runTimeTestDatas.get("New Account Name"));
			WebElement identifyElementToCheckNewAccount = seleniumReusableMethods.findElement(webDriver, "xpath",
					verifyNewAccount);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "N", "New Account Check",
					identifyElementToCheckNewAccount, "isDisplayed");

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform account creation. " + e);
			e.printStackTrace();
			methodStat = false;
		}
		return methodStat;

	}
}
