package com.automation.pageObject;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;

import java.io.IOException;

@Component
public class SalesForceLoginPage extends BaseClass {

	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();
	ExtendReportClass extendReportClass = new ExtendReportClass();

	private WebDriver webDriver;

	public SalesForceLoginPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(xpath = ".//*[@type='email']")
	public WebElement salesForceUserName;

	@FindBy(xpath = ".//*[@type='password']")
	public WebElement salesForcePassword;

	@FindBy(xpath = ".//*[@name='Login']")
	public WebElement loginButton;

	/**
	 * @author Wisefinch Menaka
	 * @see To perform Login to Sales force webpage
	 * 
	 * @param extentReport    --> Extent Report instance
	 * @param extentTest-->   Extent Report instance
	 * @param validationPoint --> Y-- Screen shot will be added / N -- Screen shot
	 *                        wont be added
	 */
	public void loginToSalseForceWebPage(ExtentReports extentReport, ExtentTest extentTest, String validationPoint) {

		String userName = properties.getProperty("UserName");
		String password = properties.getProperty("Password");

		try {
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "User Name",
					salesForceUserName, userName);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint, "Password",
					salesForcePassword, password);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Login Button",
					loginButton);
		} catch (ThrowNewException | IOException e) {
			extendReportClass.reportFail(webDriver, extentTest, "Exception when trying to perform login. " + e);
			e.printStackTrace();
		}

	}

}
