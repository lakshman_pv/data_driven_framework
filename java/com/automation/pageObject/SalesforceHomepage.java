package com.automation.pageObject;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

@Component
public class SalesforceHomepage extends BaseClass {

	private WebDriver webDriver;

	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();
	ExtendReportClass extendReportClass = new ExtendReportClass();

	public SalesforceHomepage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(xpath = ".//*[@class='slds-icon-waffle']")
	public WebElement searchAppAndItemsIcon;

	@FindBy(xpath = ".//*[@class='slds-size_medium']//*[@type='search']")
	public WebElement searchAppAndItemsInputBox;

	@FindBy(xpath = ".//*[@class='slds-size_small']//b[contains(text(),'Accounts')]")
	public WebElement selectAppAccounts;

	@FindBy(xpath = ".//*[@class='slds-size_small']//b[contains(text(),'Opportunities')]")
	public WebElement electAppOpportunities;

	@FindBy(xpath = ".//*[@class='slds-size_small']//b[contains(text(),'Products')]")
	public WebElement selectAppProducts;

	@FindBy(xpath = ".//*[@class='slds-size_small']//b[contains(text(),'Billings')]")
	public WebElement selectAppBillings;

	@FindBy(xpath = ".//*[@class='slds-size_small']//b[contains(text(),'Reports')]")
	public WebElement selectAppReports;

	public String selectAppFromHomePage = ".//*[@class='slds-size_small']//b[contains(text(),'%s')]";

	/**
	 * @author Wisefinch Menaka
	 * @see To select a from Search app and item section
	 * @param extentReport
	 * @param extentTest
	 * @param validationPoint
	 * @param appName         --> App name as displayed in Sales Force web page
	 */
	public void launchSalesForceApp(ExtentReports extentReport, ExtentTest extentTest, String validationPoint,
			String appName) {
		try {
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint,
					"Search App And Item Icon", searchAppAndItemsIcon);
			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, validationPoint,
					"Search App And Item Icom", searchAppAndItemsInputBox, appName);

			String xpathToSelectApp = selectAppFromHomePage.replaceAll("%s", appName);

			WebElement launchApp = seleniumReusableMethods.findElement(webDriver, "xpath", xpathToSelectApp);

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, validationPoint, "Launch " + appName,
					launchApp);

			seleniumReusableMethods.refresh(webDriver, extentReport, extentTest, validationPoint);
			seleniumReusableMethods.refresh(webDriver, extentReport, extentTest, validationPoint);

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest, "Exception when trying to perform login. " + e);
			e.printStackTrace();
		}
	}
}
