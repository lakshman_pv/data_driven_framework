package com.automation.letsMakeItSimplePOM;

import org.openqa.selenium.WebDriver;
import com.automation.pageObject.SalesForceLoginPage;
import com.automation.pageObject.SalesForceOpportunitiesPage;
import com.automation.pageObject.SalesforceHomepage;
import com.automation.pageObject.SalesForceAccountsPage;

public class InitPage extends BaseClass {

	public InitPage(WebDriver webDriver) {
		BaseClass.salesForceLoginPage = new SalesForceLoginPage(webDriver);
		BaseClass.SalesForceAccountsPage = new SalesForceAccountsPage(webDriver);
		BaseClass.salesforceHomepage = new SalesforceHomepage(webDriver);
		BaseClass.salesForceOpportunitiesPage = new SalesForceOpportunitiesPage(webDriver);
	}

}
