package com.automation.commonFunctionalities;

import org.springframework.stereotype.Component;

/**
 * 
 * @author Wisefinch Menaka
 *
 */
@Component
public class ThrowNewException extends Exception {
	String capturedError;
	private static final long serialVersionUID = 1L;

	/**
	 * @author Wisefinch Menaka
	 * @see To throw user defined exception
	 * @param errorText --> Error text that should be captured
	 */
	public ThrowNewException(String errorText) {
		// TODO Auto-generated constructor stub
		this.capturedError = "Exception Text : " + errorText;
		System.out.println("############ Error : " + capturedError);
	}

	/**
	 * @author Wisefinch Menaka
	 * @return --> Return the user defined error message text
	 */
	public String getErrorMessage() {
		return capturedError;
	}
}
