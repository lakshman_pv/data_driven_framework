package com.automation.commonFunctionalities;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.poi.util.SystemOutLogger;
import org.apache.xmlbeans.impl.xb.xsdschema.Attribute.Use;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

@Component
public class SeleniumReusableMethods extends BaseClass {
	protected static HashMap<String, String> xpathDetails = new HashMap<>();

	public CommenReusableMethods commonReusableMethods = new CommenReusableMethods();

	public ExtendReportClass extentReportClass = new ExtendReportClass();

	public ExtentReports reports;

	/**
	 * @param locator --> To pass whole locator value with by reference
	 * @return --> Return identified element
	 * @author Wisefinch Menaka
	 * @see Fluent wait used here to find element . It will wait for 180 seconds ,
	 *      for each 15 seconds it will check for element. If element present it
	 *      will take and proceed further or will exit after 180 seconds
	 */

	public WebElement fluentWait(WebDriver webDriver, final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(120))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(Throwable.class);

		WebElement identifiedElement = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				WebElement identifiedElement = driver.findElement(locator);

				if (identifiedElement != null) {
					// System.out.println("********** Element Identified");
					return identifiedElement;
				} else {
					// System.out.println("********** Element not Identified");
					identifiedElement = null;
				}
				return identifiedElement;
			}
		});
		return identifiedElement;
	};

	public WebElement customeFluentWaitAndCheckElement(WebDriver webDriver, final By locator, int waitTime,
			int poolingTime) {

		WebElement identifiedElement = null;

		try {

			Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(waitTime))
					.pollingEvery(Duration.ofSeconds(poolingTime)).ignoring(Throwable.class);

			identifiedElement = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					WebElement identifiedElement = driver.findElement(locator);

					if (identifiedElement != null) {
						// System.out.println("********** Element Identified");
						return identifiedElement;
					} else {
						// System.out.println("********** Element not Identified");
						identifiedElement = null;
					}
					return identifiedElement;
				}
			});

		} catch (Exception e) {
			System.out.println("There are no such element , so gonna return null");
		}
		return identifiedElement;
	}

	/**
	 * @param webDriver --> Current instance of {@link WebDriver}
	 * @param element   --> Element that needs to be identified
	 * @return --> Return the element when the element is clickable.
	 * @author Wisefinch Menaka
	 */
	public WebElement fluentWaitCheckElementToBeClickable(WebDriver webDriver, WebElement element) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(120))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(Throwable.class);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * @param webDriver
	 * @param element
	 * @return
	 * @author Menaka
	 * @see to check element invisibility
	 */
	public Boolean fluentWaitCheckElementInVisibility(WebDriver webDriver, WebElement element) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(120))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(Throwable.class);
		return wait.until(ExpectedConditions.invisibilityOf(element));
	}

	/**
	 * @param webDriver --> Current instance of {@link WebDriver}
	 * @param element   --> Element that needs to be identified
	 * @return --> Return the element when the element is clickable.
	 * @author Wisefinch Menaka
	 */
	public WebElement fluentWaitCheckElementVisibility(WebDriver webDriver, WebElement element) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(120))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(Throwable.class);
		return wait.until(ExpectedConditions.visibilityOf(element));
	}

	;

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param url             --> URL value to launch
	 * @return --> If method executed successfully it returns true / else false
	 * @author Wisefinch Menaka
	 * @see Launch the given URL in browser
	 */
	public Boolean launchWebDriver(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String url) {
		// WebDriverManager.chromedriver().setup();
		Boolean methodExecuted = true;
		try {
			webDriver.navigate().to(url);
			extentReportClass.reportPass(webDriver, extentTest, validationPoint, url + " Launched Successfully");
		} catch (Throwable e) {
			e.printStackTrace();
			extentReportClass.reportFail(webDriver, extentTest,
					url + " is not Launched. The Error here is " + e.toString());
			System.out.println("********** Exception during webdriver lauch " + e);
			close(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 * @param waitTime        --> How much time current execution wait.
	 * @return
	 * @author Wisefinch Menaka
	 * @see To perform wait action
	 */
	public Boolean wait(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest, String validationPoint,
			String waitTime) {
		// WebDriverManager.chromedriver().setup();
		Boolean methodExecuted = true;
		try {
			Thread.sleep(Integer.parseInt(waitTime));
		} catch (Throwable e) {
			e.printStackTrace();
			extentReportClass.reportFail(webDriver, extentTest,
					url + " is not Launched. The Error here is " + e.toString());
			System.out.println("********** Exception during webdriver lauch " + e);
			close(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver    --> Current instance of {@link WebDriver}
	 * @param extentReport --> Current instance of {@link ExtentReports}
	 * @param extentTest   --> Current instance of {@link ExtentTest}
	 * @param url          --> URL value to launch
	 * @return --> If method executed successfully it returns true / else false
	 * @author Wisefinch Menaka
	 * @see To quit the webdriver
	 */
	public Boolean quit(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest, String validationPoin) {
		Boolean methodExecuted = true;
		try {
			// System.out.println("********** Quit");
			webDriver.quit();
			// extentReport.reportInfo(extendTest, "Test case Completed");
		} catch (Throwable e) {
			e.printStackTrace();
			extentReportClass.reportFail(webDriver, extentTest,
					" Unable to perform quit on webdriver. The Exception here is" + e.toString());
			System.out.println("********** Exception during web driver quit " + e);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver    --> Current instance of {@link WebDriver}
	 * @param extentReport --> Current instance of {@link ExtentReports}
	 * @param extentTest   --> Current instance of {@link ExtentTest}
	 * @return --> If method executed successfully it returns true / else false
	 * @author Wisefinch Menaka
	 * @see To close the webdriver
	 */
	public Boolean close(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint) {
		Boolean methodExecuted = true;
		try {
			// System.out.println("********** close");
			webDriver.close();
			// extentReport.reportInfo(extendTest, "Test case Completed");
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during web driver close " + e);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to perform mouseover
	 * @param condition       --> Take screen shot "Y" / "N"
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To verify element visiblity / enable status / select status
	 */
	public Boolean verify(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element, String condition)
			throws IOException, ThrowNewException {

		boolean methodExecuted = true;
		try {
			// System.out.println("locatorValue " + locatorValue);
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);

			// System.out.println("********** clickableElement : " + clickableElement);
			if (clickableElement != null) {

				// System.out.println("**********" + condition.toLowerCase());
				if (element != null) {
					switch (condition.toLowerCase()) {
					case "isdisplayed":
						if (!element.isDisplayed()) {
							throw new ThrowNewException(elementName + "Is not displayed");
						} else {
							// System.out.println("********** Element Displayed");
							extentReportClass.reportPass(webDriver, extentTest, validationPoint,
									elementName + " is displayed");
						}
						break;
					case "notdisplayed":
						if (element.isDisplayed()) {
							throw new ThrowNewException(elementName + " Is displayed");
						} else {
							// System.out.println("********** Element Displayed");
							extentReportClass.reportPass(webDriver, extentTest, validationPoint,
									elementName + " is displayed");
						}
						break;
					case "isenabled":
						if (!element.isEnabled()) {
							throw new ThrowNewException(elementName + "Is not enabled");
						} else {
							// System.out.println("********** Element Enabled");
							extentReportClass.reportPass(webDriver, extentTest, validationPoint,
									elementName + " is enabled");
						}
						break;
					case "isselected":
						if (!element.isSelected()) {
							throw new ThrowNewException(elementName + "Is not selected");
						} else {
							// System.out.println("********** Element Selected");
							extentReportClass.reportPass(webDriver, extentTest, validationPoint,
									elementName + " is selected");
						}
						break;
					case "notselected":
						if (!element.isSelected()) {
							System.out.println("********** Element not Selected");
							extentReportClass.reportPass(webDriver, extentTest, validationPoint,
									elementName + " is not selected");

						} else {
							throw new ThrowNewException(elementName + "Is not selected");
						}
						break;

					default:
						throw new ThrowNewException(condition + "Is not a valid verify condition");
					}
				}
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during web driver close " + e);
			extentReportClass.reportFail(webDriver, extentTest, "Exception on verify() method " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to perform mouseover
	 * @param condition       --> Take screen shot "Y" / "N"
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To verify element visiblity / enable status / select status
	 */
	public Boolean verifyElement(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, String element, String condition)
			throws IOException, ThrowNewException {

		methodexecutionStat = true;
		try {
			// System.out.println("locatorValue " + locatorValue);

			List<WebElement> clickableElements = findElements(webDriver, "xpath", element, 15,1);
			int size = clickableElements.size();
			System.out.println("size: " + size);
			// System.out.println("********** clickableElement : " + clickableElement);

			if (size > 0) {
				System.out.println("Element Present");
				WebElement clickableElement = findElement(webDriver, "xpath", element);

				if (clickableElement != null) {
					methodexecutionStat = true;
				} else {
					methodexecutionStat = false;
				}

			} else {
				System.out.println("Element not Present");
				methodexecutionStat = false;
			}

		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during web driver close " + e);
			extentReportClass.reportFail(webDriver, extentTest, "Exception on verify() method " + e.toString());
			/* quit(webDriver, extentReport, extentTest, validationPoint); */
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodexecutionStat = false;
		}
		return methodexecutionStat;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to perform click
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To perform click on the webelement
	 */
	public Boolean click(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest, String validationPoint,
			String elementName, WebElement element) throws IOException, ThrowNewException {
		Boolean methodExecuted = true;
		try {

			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);

			if (clickableElement != null) {
				element.click();
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;

		} catch (ElementNotInteractableException e) {

			try {
				Actions actions = new Actions(webDriver);
				actions.click(element).build().perform();
				System.out.println("********** Clicked using actions");
			} catch (ElementNotInteractableException e1) {
				JavascriptExecutor executor = (JavascriptExecutor) webDriver;
				executor.executeScript("arguments[0].click();", element);
				System.out.println("********** Clicked using java script");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("********** Exception during click functionality " + e);

			extentReportClass.reportFail(webDriver, extentTest,
					" exception when trying to perform click on " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to perform sendKey
	 * @param valueToEnter    --> Value that needs to be entered
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To perfome send key action
	 */
	public Boolean sendKey(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element, String valueToEnter)
			throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		String dataFromRunTimeTestData = runTimeTestDatas.get(valueToEnter);

		if (dataFromRunTimeTestData != null) {
			valueToEnter = dataFromRunTimeTestData;
		} else if (valueToEnter.contains("TimeStamp")) {
			String timeStampValue = CommenReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss");
			valueToEnter = valueToEnter.replace("TimeStamp", timeStampValue);
		}

		try {
			if (webDriver.toString().contains("AndroidDriver") || webDriver.toString().contains("AppiumDriver")
					|| webDriver.toString().contains("IOSDriver")) {
				element.sendKeys(valueToEnter);
			} else {
				WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);

				if (clickableElement != null) {

					element.click();
					element.clear();
					element.sendKeys(Keys.CONTROL + "A");
					element.sendKeys(Keys.DELETE);
					element.sendKeys(valueToEnter);

					/*
					 * Thread.sleep(10000); System.out.println("*********** " + element);
					 * element.sendKeys(valueToEnter);
					 */
					commonReusableMethods.saveRunTimeTestData(elementName, valueToEnter);

					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							valueToEnter + " is entered on " + elementName);
				} else {
					throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
				}
			}
		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during enter functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to entered the value " + valueToEnter + " to " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementName                --> Name of the element [This information
	 *                                   is gathered here to provide intractable
	 *                                   extend report]
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * @param xpathParameterValue        --> If xpath need some parameter value,
	 *                                   that needs to be given here.
	 * @param valueToEnter               --> Value to be entered to a element
	 * @return --> If method executed successfully it returns true / else false
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To perfome send key action
	 */
	public Boolean sendKeyAndEnter(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element, String valueToEnter)
			throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		String[] elementNameToSplit = elementName.split("_");
		if (elementNameToSplit.length > 1) {
			elementName = elementNameToSplit[1];
		}

		String dataFromRunTimeTestData = runTimeTestDatas.get(valueToEnter);

		if (dataFromRunTimeTestData != null) {
			valueToEnter = dataFromRunTimeTestData;
		} else if (valueToEnter.contains("TimeStamp")) {
			String timeStampValue = CommenReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss");
			valueToEnter = valueToEnter.replace("TimeStamp", timeStampValue);
		}

		try {
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);
			if (clickableElement != null) {
				if (webDriver.toString().contains("AndroidDriver") || webDriver.toString().contains("AppiumDriver")
						|| webDriver.toString().contains("IOSDriver")) {
					clickableElement.sendKeys(valueToEnter);
				} else {
					clickableElement.click();
					clickableElement.sendKeys(Keys.CONTROL + "A");
					clickableElement.sendKeys(Keys.DELETE);
					// element.sendKeys(valueToEnter+Keys.ENTER+Keys.ENTER);
					clickableElement.sendKeys(valueToEnter + "\n");

					commonReusableMethods.saveRunTimeTestData(elementName, valueToEnter);
				}
				extentReportClass.reportPass(webDriver, extentTest, validationPoint,
						valueToEnter + " is entered on " + elementName);
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during enter functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to entered the value " + valueToEnter + " to " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver   --> Current instance of {@link WebDriver}
	 * @param locatorType --> Locator type value. Values should be xpath / classname
	 *                    / cssselector / id / linktext / name / partiallinktext /
	 *                    tagname
	 * @param value       --> Locator value
	 * @return --> Return a webelement
	 * @author Wisefinch Menaka
	 * @see To find a element from the current page
	 */
	public WebElement findElement(WebDriver webDriver, String locatorType, String value) {
		WebElement identifiedElement = null;

		System.out.println("Looking for " + value);

		try {
			switch (locatorType.toLowerCase()) {
			case "xpath":
				identifiedElement = fluentWait(webDriver, By.xpath(value));
				break;
			case "classname":
				identifiedElement = fluentWait(webDriver, By.className(value));
				break;
			case "cssselector":
				identifiedElement = fluentWait(webDriver, By.cssSelector(value));
				break;
			case "id":
				identifiedElement = fluentWait(webDriver, By.id(value));
				break;
			case "linktext":
				identifiedElement = fluentWait(webDriver, By.linkText(value));
				break;
			case "name":
				identifiedElement = fluentWait(webDriver, By.name(value));
				break;
			case "partiallinktext":
				identifiedElement = fluentWait(webDriver, By.partialLinkText(value));
				break;
			case "tagname":
				identifiedElement = fluentWait(webDriver, By.tagName(value));
				break;
			default:
				throw new ThrowNewException("Invalid Selector Type");
			}
		} catch (ThrowNewException e) {
			e.printStackTrace();
			System.out.println("********** Exception During Find Element : " + e);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Find Element : " + e);
		}

		return identifiedElement;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to select
	 * @param selectionType   --> Type of select
	 * @param selectValue     --> Value to select
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To perform select on drop down list
	 */
	public boolean select(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element, String selectionType, String selectValue)
			throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);

			if (clickableElement != null) {
				Select selectDropDown = new Select(element);

				switch (selectionType.toLowerCase()) {
				case "selectbyindex":
					selectDropDown.selectByIndex(Integer.parseInt(selectValue));
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "selectbyvalue":
					selectDropDown.selectByValue(selectValue);
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "selectbyvisibletext":
					selectDropDown.selectByVisibleText(selectValue);
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "deselectall":
					selectDropDown.deselectAll();
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "deselectbyindex":
					selectDropDown.deselectByIndex(Integer.parseInt(selectValue));
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "deselectbyvalue":
					selectDropDown.deselectByValue(selectValue);
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				case "deselectbyvisibletext":
					selectDropDown.deselectByVisibleText(selectValue);
					extentReportClass.reportPass(webDriver, extentTest, validationPoint,
							selectValue + " is selected for the element " + elementName);
					break;
				default:
					throw new ThrowNewException(
							selectionType + " is not a valid option. Please provide valid select type");
				}

			} else {
				throw new ThrowNewException("Unable to location " + elementName + " in current webpage.");
			}
		} catch (

		ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during select functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to select the value " + selectValue + " to " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementName                --> Name of the element [This information
	 *                                   is gathered here to provide intractable
	 *                                   extend report]
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * @param xpathParameterValue        --> If xpath need some parameter value,
	 *                                   that needs to be given here.
	 * @return
	 * @throws ThrowNewException
	 * @throws IOException
	 * @author Wisefinch Menaka
	 * @see To perform mouseOver action
	 */
	public boolean mouseOver(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			WebElement clickableElement = fluentWaitCheckElementVisibility(webDriver, element);
			if (clickableElement != null) {
				Actions performAction = new Actions(webDriver);
				performAction.moveToElement(clickableElement).build().perform();
				extentReportClass.reportPass(webDriver, extentTest, validationPoint,
						"Mouse over done successfully on the element " + elementName);
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during mouse over functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Exception during mouse over on the element " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementName                --> Name of the element [This information
	 *                                   is gathered here to provide intractable
	 *                                   extend report]
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * @param xpathParameterValue        --> If xpath need some parameter value,
	 *                                   that needs to be given here.
	 * @return
	 * @throws ThrowNewException
	 * @throws IOException
	 * @author Wisefinch Menaka
	 * @see To perform scroll into element action
	 */
	public boolean scrollIntoElement(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element)
			throws IOException, ThrowNewException, InterruptedException {

		Boolean methodExecuted = true, elementIdentified = false;

		try {
			for (int i = 0; i < 10; i++) {
				JavascriptExecutor js = (JavascriptExecutor) webDriver;
				js.executeScript("window.scrollBy(0,300)", "");
				// System.out.println("Scrolling done");
				Thread.sleep(1);
			}

			((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during scrollIntoElement functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Exception during scroll into element action " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementName                --> Name of the element [This information
	 *                                   is gathered here to provide intractable
	 *                                   extend report]
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * @param xpathParameterValue        --> If xpath need some parameter value,
	 *                                   that needs to be given here.
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see This method will gather the text from provided xpath and store it in to
	 *      runTimeTestData hashmap.
	 */
	public Boolean getText(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element) throws IOException, ThrowNewException {

		String getExactElementName = null;
		Boolean methodExecuted = true;

		try {
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);
			if (clickableElement != null) {
				String textGatheredFromElement = clickableElement.getText();

				if (textGatheredFromElement.equalsIgnoreCase("")) {
					textGatheredFromElement = element.getAttribute("value");
				}

				System.out.println("********** textGatheredFromElement " + textGatheredFromElement);

				if (textGatheredFromElement.equalsIgnoreCase("")) {
					throw new ThrowNewException(
							"There are no values returned from getText() or getAttribute(\"value\") for the "
									+ elementName + " element.");
				}

				extentReportClass.reportInfo(webDriver, extentTest,
						textGatheredFromElement + " the text identified from the element " + elementName);

				commonReusableMethods.saveRunTimeTestData(elementName, textGatheredFromElement);

			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}
		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during getText functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to perform getText from the element " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see This is just to refresh the page
	 */
	public Boolean refresh(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			webDriver.navigate().refresh();
			Thread.sleep(5000);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during refresh functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest, "Unable to perform refresh. " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see This is just to refresh the page
	 */
	public Boolean acceptAlert(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			try {
				WebDriverWait wait = new WebDriverWait(webDriver, 10 /* timeout in seconds */);

				if (wait.until(ExpectedConditions.alertIsPresent()) == null) {
					System.out.println("*********** alert was not present");
				}
			} catch (Exception e) {
				System.out.println("*********** alert was present and accepted");
			}

			Alert alert = webDriver.switchTo().alert();
			alert.accept();
			System.out.println("*********** alert was present and accepted");

		} catch (Exception e) {
			System.out.println("********** Popup is not dismissed");
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file
	 * @param elementName     --> Element Name
	 * @param valueToEnter    --> Expected in the format of year-month-date formate.
	 *                        Ex - 2021-NOV-22
	 * @return
	 * @throws ThrowNewException
	 * @throws IOException
	 * @author Wisefinch Menaka
	 * @author Wisefinch To select date from old calender element
	 */
	@SuppressWarnings("unused")
	public boolean datePicker(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, String valueToEnter) throws ThrowNewException, IOException {

		Boolean methodExecuted = true;
		String[] requiredElements = elementName.split(",");

		String dateInputBoxElement, monthPickerXpathName, yearPickerXpathName, datePickerXpathName, date, month, year,
				datePickerElementXpath = null, monthDatePickerXpath = null, yearDatePickerXpath = null,
				dateDatePicketXpath = null;

		WebElement dateInputBox, monthPickerElement, yearPickerElement, datePickerElement;

		if (valueToEnter != null) {
			String[] dateDetails = valueToEnter.split("-");
			year = dateDetails[0];
			month = dateDetails[1];
			date = dateDetails[2];
		} else {
			throw new ThrowNewException(
					"There are no input data provided. Input data expected in the format of YYYY-MMM-DD. Eg: 2021-NOV-1");
		}

		if (requiredElements.length == 4) {
			dateInputBoxElement = requiredElements[0];
			monthPickerXpathName = requiredElements[1];
			yearPickerXpathName = requiredElements[2];
			datePickerXpathName = requiredElements[3];

			/*
			 * datePickerElementXpath = xpathDetails.get(dateInputBoxElement);
			 * monthDatePickerXpath = xpathDetails.get(monthPickerXpathName);
			 * yearDatePickerXpath = xpathDetails.get(yearPickerXpathName);
			 * dateDatePicketXpath = xpathDetails.get(datePickerXpathName);
			 */

			if (datePickerElementXpath != null) {
				String[] xpathDetails = datePickerElementXpath.split("&&");
				String locatorType = xpathDetails[0];
				String locatorValue = xpathDetails[1];
				try {
					dateInputBox = findElement(webDriver, locatorType, locatorValue);
				} catch (Throwable e) {
					throw new ThrowNewException(
							"There are exception when trying to find " + dateInputBoxElement + " ." + e);
				}
			} else {
				throw new ThrowNewException("There are no xpath added with the name " + datePickerElementXpath);
			}

		} else {
			throw new ThrowNewException("Expecting 4 elements to perform date picker action separated with ,");
		}

		try {
			if (dateInputBox != null) {
				dateInputBox.click();

				if (monthDatePickerXpath != null) {
					String[] xpathDetails = monthDatePickerXpath.split("&&");
					String locatorType = xpathDetails[0];
					String locatorValue = xpathDetails[1];
					try {
						monthPickerElement = findElement(webDriver, locatorType, locatorValue);
						if (monthPickerElement != null) {
							Select monthSelect = new Select(monthPickerElement);
							monthSelect.selectByVisibleText(month);

							if (yearDatePickerXpath != null) {
								xpathDetails = yearDatePickerXpath.split("&&");
								locatorType = xpathDetails[0];
								locatorValue = xpathDetails[1];
								try {
									yearPickerElement = findElement(webDriver, locatorType, locatorValue);
									if (yearPickerElement != null) {
										Select yearSelect = new Select(yearPickerElement);
										yearSelect.selectByVisibleText(year);

										if (dateDatePicketXpath != null) {
											xpathDetails = dateDatePicketXpath.split("&&");
											locatorType = xpathDetails[0];
											locatorValue = xpathDetails[1].replaceFirst("%s", date);
											try {
												datePickerElement = findElement(webDriver, locatorType, locatorValue);

												if (datePickerElement != null) {
													datePickerElement.click();
												} else {
													throw new ThrowNewException("Unable to locate  "
															+ datePickerXpathName + " in current screen");

												}
											} catch (Throwable e) {
												throw new ThrowNewException("There are exception when trying to find "
														+ datePickerXpathName + " ." + e);
											}
										} else {
											throw new ThrowNewException(
													"There are no xpath added with the name " + datePickerXpathName);
										}

									} else {
										throw new ThrowNewException(
												"Unable to locate  " + yearPickerXpathName + " in current screen");
									}
								} catch (Throwable e) {
									throw new ThrowNewException("There are exception when trying to find "
											+ yearPickerXpathName + " ." + e);
								}
							} else {
								throw new ThrowNewException(
										"There are no xpath added with the name " + yearPickerXpathName);
							}

						} else {
							throw new ThrowNewException(
									"Unable to locate  " + monthPickerXpathName + " in current screen");
						}

					} catch (Throwable e) {
						throw new ThrowNewException(
								"There are exception when trying to find " + monthPickerXpathName + " ." + e);
					}
				} else {
					throw new ThrowNewException("There are no xpath added with the name " + monthDatePickerXpath);
				}

			} else {
				throw new ThrowNewException("Unable to locate  " + dateInputBoxElement + " in current screen");
			}
		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;

		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during date picker select functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest, "Unable to select date from datepicker" + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @param webDriver   --> Current instance of {@link WebDriver}
	 * @param locatorType --> Locator type value. Values should be xpath / classname
	 *                    / cssselector / id / linktext / name / partiallinktext /
	 *                    tagname
	 * @param value       --> Locator value
	 * @return --> Return a webelement
	 * @author Wisefinch Lakshman
	 * @see To find a element from the current page
	 */
	@SuppressWarnings("null")
	public List<WebElement> findElements(WebDriver webDriver, String locatorType, String value, int waitTime,
			int poolingTime) {
		List<WebElement> identifiedElement = null;
		WebElement identifyElement = null;
		try {
			switch (locatorType.toLowerCase()) {
			case "xpath":

				identifiedElement = webDriver.findElements(By.xpath(value));

				break;
			case "classname":

				identifiedElement = webDriver.findElements(By.className(value));

				break;
			case "cssselector":

				identifiedElement = webDriver.findElements(By.cssSelector(value));

				break;
			case "id":

				identifiedElement = webDriver.findElements(By.id(value));

				break;
			case "linktext":

				identifiedElement = webDriver.findElements(By.linkText(value));

				break;
			case "name":

				identifiedElement = webDriver.findElements(By.name(value));

				break;
			case "partiallinktext":

				identifiedElement = webDriver.findElements(By.partialLinkText(value));

				break;
			case "tagname":

				identifiedElement = webDriver.findElements(By.tagName(value));

				break;
			default:
				throw new ThrowNewException("Invalid Selector Type");
			}
		} catch (ThrowNewException e) {
			e.printStackTrace();
			System.out.println("********** Exception During Find Elements : " + e);
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception During Find Elements : " + e);
		}

		return identifiedElement;
	}

	/**
	 * @param webDriver         --> Current instance of {@link WebDriver}
	 * @param extentReport      --> Current instance of {@link ExtentReports}
	 * @param extentTest        --> Current instance of {@link ExtentTest}
	 * @param validationPoint   --> If yes screenshot will be attached , else only
	 *                          information will be added to reportTest html file.
	 * @param elementName       --> Name of the element [This information is
	 *                          gathered here to provide intractable extend report]
	 * @param switchFrameNumber --> The frame number that we want to switch
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see To perform switch frame
	 */
	public Boolean switchFrame(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String switchFrameNumber) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		try {

			WebElement frame = findElement(webDriver, "tagname", "iframe");
			List<WebElement> listOfFrames = webDriver.findElements(By.tagName("iframe"));
			int size = listOfFrames.size();
			List<WebElement> listOfFrame = webDriver.findElements(By.tagName("frame"));
			int sizeOfFrame = listOfFrame.size();
			System.out.println("********** listOfFrames " + listOfFrames.size());

			if (size > 0) {
				// System.out.println("********* switchFrameNumber " + switchFrameNumber);
				webDriver.switchTo().frame(Integer.parseInt(switchFrameNumber));
				System.out.println("********* Frame switched");
			} else {
				System.out.println("********* There are no frame identified in this screen");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("********** Exception during switch frame " + e);
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see After switching to frame , we can use it to come out of frame
	 */
	public Boolean switchToDefaultContent(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		try {
			Thread.sleep(5000);
			webDriver.switchTo().defaultContent();
			Thread.sleep(5000);
			System.out.println("********** Switched to default content");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("********** Exception during switch to default content " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					" exception when trying to perform seitch to default content" + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @return --> If method executed successfully it returns true / else false
	 * @throws ThrowNewException
	 * @throws IOException
	 * @author Wisefinch Menaka
	 * @see To perform double click on particular element
	 */
	public Boolean doubleClick(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);

			if (clickableElement != null) {
				Actions actions = new Actions(webDriver);
				actions.moveToElement(clickableElement).doubleClick(clickableElement).build().perform();
				System.out.println("**********double Clicked using actions");

				extentReportClass.reportPass(webDriver, extentTest, validationPoint, elementName + " is clicked");
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("********** Exception during click functionality " + e);

			extentReportClass.reportFail(webDriver, extentTest,
					" exception when trying to perform click on " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to gather text from
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see Use this method to perform get text action
	 */
	public String getTextForCode(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String elementName, WebElement element) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		String textGatheredFromElement = null;
		try {

			if (element != null) {
				textGatheredFromElement = element.getText();

				if (textGatheredFromElement.equalsIgnoreCase("")) {
					textGatheredFromElement = element.getAttribute("value");
				}

				System.out.println("********** textGatheredFromElement " + textGatheredFromElement);

				if (textGatheredFromElement.equalsIgnoreCase("")) {
					throw new ThrowNewException(
							"There are no values returned from getText() or getAttribute(\"value\") for the "
									+ elementName + " element.");
				}

			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}

		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during getText functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to perform getText from the element " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return textGatheredFromElement;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param elementName     --> Name of the element [This information is gathered
	 *                        here to provide intractable extend report]
	 * @param element         --> Element to perform clear
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 * @author Wisefinch Menaka
	 * @see Just to clear the input box
	 */
	public Boolean clear(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest, String validationPoint,
			String elementName, WebElement element) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;

		try {
			WebElement clickableElement = fluentWaitCheckElementToBeClickable(webDriver, element);
			if (clickableElement != null) {
				if (webDriver.toString().contains("AndroidDriver") || webDriver.toString().contains("AppiumDriver")
						|| webDriver.toString().contains("IOSDriver")) {
					clickableElement.clear();
				} else {
					clickableElement.sendKeys(Keys.CONTROL + "A");
					clickableElement.sendKeys(Keys.DELETE);
				}
			} else {
				throw new ThrowNewException("Unable to locate  " + elementName + " in current screen");
			}
			extentReportClass.reportPass(webDriver, extentTest, validationPoint,
					"Cleared The Input Box " + elementName);
		} catch (ThrowNewException e) {
			extentReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during clear functionality " + e);
			extentReportClass.reportFail(webDriver, extentTest,
					"Unable to clear values from " + elementName + ". " + e.toString());
			quit(webDriver, extentReport, extentTest, validationPoint);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	public static boolean tap(AppiumDriver<WebElement> driver, AndroidElement element) {

		TapOptions.tapOptions();
		new AndroidTouchAction(driver).tap(TapOptions.tapOptions().withElement(ElementOption.element(element)))
				.perform();
		return false;
	}

	public static boolean tapUsingCoorinates(AppiumDriver<WebElement> driver, AndroidElement element) {
		TapOptions.tapOptions();
		new AndroidTouchAction(driver).tap(PointOption.point(0, 0)).perform();
		return false;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param numberOfTimes   --> Number of time the swipe up action to occur
	 * @return
	 * @author Wisefinch Menaka
	 * @see To perform swipt up action
	 */
	public boolean swipeUp(AppiumDriver driver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String numberOfTimes) {
		Boolean methodExecuted = true;

		Dimension size = driver.manage().window().getSize();
		// System.out.println("********** size : " + size);

		int startX = 0;
		int endX = 0;
		int startY = 0;
		int endY = 0;

		int count = Integer.parseInt(numberOfTimes);

		for (int i = 0; i < count; i++) {
			endY = (int) (size.height * 0.70);
			startY = (int) (size.height * 0.30);
			startX = (size.width / 2);
			/*
			 * System.out.println("********** endY   : " + endY);
			 * System.out.println("********** startY : " + startY);
			 * System.out.println("********** startX : " + startX);
			 */

			PointOption startPoint = new PointOption().withCoordinates(startX, startY);
			PointOption endPoint = new PointOption().withCoordinates(startX, endY);

			/*
			 * new TouchAction(driver).press(startX,
			 * startY).waitAction(Duration.ofMillis(duration)).moveTo(startX, endY)
			 * .release().perform();
			 */

			TouchAction touchAction = new TouchAction<>(driver);
			touchAction.press(endPoint).waitAction(new WaitOptions().withDuration(Duration.ofSeconds(1)))
					.moveTo(startPoint).release().perform();

			// System.out.println("********** Swipe Up");
		}
		return true;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param numberOfTimes   --> Number of time the swipe down action to occur
	 * @return
	 * @author Wisefinch Menaka
	 * @see To perform swipt Down action
	 */
	public boolean swipeDown(AppiumDriver driver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String numberOfTimes) {
		Boolean methodExecuted = true;

		Dimension size = driver.manage().window().getSize();
		// System.out.println("********** size : " + size);

		int startX = 0;
		int endX = 0;
		int startY = 0;
		int endY = 0;

		int count = Integer.parseInt(numberOfTimes);

		for (int i = 0; i < count; i++) {
			endY = (int) (size.height * 0.70);
			startY = (int) (size.height * 0.30);
			startX = (size.width / 2);
			/*
			 * System.out.println("********** endY   : " + endY);
			 * System.out.println("********** startY : " + startY);
			 * System.out.println("********** startX : " + startX);
			 */

			PointOption startPoint = new PointOption().withCoordinates(startX, startY);
			PointOption endPoint = new PointOption().withCoordinates(startX, endY);

			startY = (int) (size.height * 0.70);
			endY = (int) (size.height * 0.30);
			startX = (size.width / 2);
			/*
			 * new TouchAction(driver).press(startX,
			 * startY).waitAction(Duration.ofMillis(duration)).moveTo(startX, endY)
			 * .release().perform();
			 */

			TouchAction touchAction = new TouchAction<>(driver);
			touchAction.press(startPoint).waitAction(new WaitOptions().withDuration(Duration.ofSeconds(1)))
					.moveTo(endPoint).release().perform();
			// System.out.println("********** Swipe Down");
		}
		return true;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param numberOfTimes   --> Number of time the swipe right action to occur
	 * @return
	 * @author Wisefinch Menaka
	 * @see To perform swipt Right action
	 */
	public boolean swipeRight(AppiumDriver driver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String numberOfTimes) {
		Boolean methodExecuted = true;

		Dimension size = driver.manage().window().getSize();
		// System.out.println("********** size : " + size);

		int startX = 0;
		int endX = 0;
		int startY = 0;
		int endY = 0;

		int count = Integer.parseInt(numberOfTimes);

		for (int i = 0; i < count; i++) {
			startY = (int) (size.height / 2);
			startX = (int) (size.width * 0.90);
			endX = (int) (size.width * 0.05);

			PointOption startPoint = new PointOption().withCoordinates(startX, startY);
			PointOption endPoint = new PointOption().withCoordinates(endX, startY);

			/*
			 * new TouchAction(driver).press(startX,
			 * startY).waitAction(Duration.ofMillis(duration)).moveTo(endX, startY)
			 * .release().perform();
			 */

			TouchAction touchAction = new TouchAction<>(driver);
			touchAction.press(startPoint).waitAction(new WaitOptions().withDuration(Duration.ofSeconds(1)))
					.moveTo(endPoint).release().perform();
			/* System.out.println("********** Swipe Right"); */
		}
		return true;
	}

	/**
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param numberOfTimes   --> Number of time the swipe Left action to occur
	 * @return
	 * @author Wisefinch Menaka
	 * @see To perform swipt Left action
	 */
	public boolean swipeLeft(AppiumDriver driver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String numberOfTimes) {
		Boolean methodExecuted = true;

		Dimension size = driver.manage().window().getSize();
		// System.out.println("********** size : " + size);

		int startX = 0;
		int endX = 0;
		int startY = 0;
		int endY = 0;

		int count = Integer.parseInt(numberOfTimes);

		for (int i = 0; i < count; i++) {
			startY = (int) (size.height / 2);
			startX = (int) (size.width * 0.05);
			endX = (int) (size.width * 0.90);
			/*
			 * System.out.println("********** endY   : " + endY);
			 * System.out.println("********** startY : " + startY);
			 * System.out.println("********** startX : " + startX);
			 */

			PointOption startPoint = new PointOption().withCoordinates(startX, startY);
			PointOption endPoint = new PointOption().withCoordinates(endX, startY);

			/*
			 * new TouchAction(driver).press(startX,
			 * startY).waitAction(Duration.ofMillis(duration)).moveTo(endX, startY)
			 * .release().perform();
			 */

			TouchAction touchAction = new TouchAction<>(driver);
			touchAction.press(startPoint).waitAction(new WaitOptions().withDuration(Duration.ofSeconds(1)))
					.moveTo(endPoint).release().perform();
			// System.out.println("********** Swipe Left");
		}
		return true;
	}

	/**
	 * @return --> Return init webdriver reference
	 * @author Wisefinch Menaka
	 * @see To inti webdriver
	 */
	public WebDriver initWebDriver(String driverName, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint) {
		WebDriver webDriver = null;
		try {
			if (driverName != null) {
				switch (driverName.toLowerCase()) {
				case "chrome":
					WebDriverManager.chromedriver().setup();
					ChromeOptions chromeOption = new ChromeOptions();
					chromeOption.addArguments("--disable-notifications");
					webDriver = new ChromeDriver(chromeOption);
					webDriver.manage().window().maximize();
					break;
				case "firefox":
					WebDriverManager.firefoxdriver().setup();
					FirefoxOptions firefoxOptions = new FirefoxOptions();
					firefoxOptions.addArguments("--disable-notifications");
					webDriver = new FirefoxDriver(firefoxOptions);
					webDriver.manage().window().maximize();
					break;
				case "edge":
					WebDriverManager.edgedriver().setup();
					webDriver = new EdgeDriver();
					webDriver.manage().window().maximize();
					break;
				case "ie":
					WebDriverManager.iedriver().setup();
					webDriver = new InternetExplorerDriver();
					webDriver.manage().window().maximize();
					break;
				case "opera":
					WebDriverManager.operadriver().setup();
					webDriver = new OperaDriver();
					webDriver.manage().window().maximize();
					break;
				default:
					WebDriverManager.chromedriver().setup();
					ChromeOptions option1 = new ChromeOptions();
					option1.addArguments("--disable-notifications");
					webDriver = new ChromeDriver(option1);
					webDriver.manage().window().maximize();
					break;
				}
			} else {
				WebDriverManager.chromedriver().setup();
				ChromeOptions option = new ChromeOptions();
				option.addArguments("--disable-notifications");
				webDriver = new ChromeDriver(option);
				webDriver.manage().window().maximize();
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception in launch WebDriver " + e);
			extentReportClass.reportFail(webDriver, extentTest, " Exception in launch WebDriver " + e);
			extentReportClass.endReport(extentReport, extentTest, validationPoint);
		}
		return webDriver;

	}

}
