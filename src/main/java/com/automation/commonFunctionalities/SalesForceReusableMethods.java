package com.automation.commonFunctionalities;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

@Component
public class SalesForceReusableMethods extends BaseClass {

	@Autowired
	protected SeleniumReusableMethods seleniumReusableMethods;

	@Autowired
	protected ExtendReportClass extendReportClass;

	/**
	 * @author Wisefinch Menaka
	 * @see Remove all the columns present under preview section
	 * 
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementName                --> Name of the element [This information
	 *                                   is gathered here to provide intractable
	 *                                   extend report]
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * 
	 * @param xpathParameterValue        --> If xpath need some parameter value,
	 *                                   that needs to be given here.
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 */
	public Boolean removeAllColumnsFromPreviewSectionsOfReports(WebDriver webDriver, ExtentReports extentReport,
			ExtentTest extentTest, String validationPoint) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		try {

			WebDriver xpathDetails = null;
			String xpathDetailsOfPreviewColumn = xpathDetails.getCurrentUrl();
			String[] split = xpathDetailsOfPreviewColumn.split("&&");
			String previewMenuLocatorType = split[0];
			String previewMenulocatorValue = split[1];

			WebElement previewColumnMenu = seleniumReusableMethods.findElement(webDriver, previewMenuLocatorType,
					previewMenulocatorValue);
			if (previewColumnMenu != null) {
				listOfWebElement = seleniumReusableMethods.findElements(webDriver, previewMenuLocatorType,
						previewMenulocatorValue, 60, 1);

				int numberOfColumns = listOfWebElement.size();

				// System.out.println("********** numberOfColumns " + numberOfColumns);
				while (numberOfColumns != 1) {
					Thread.sleep(5000);
					// System.out.println("********** : " + previewColumnMenu);
					previewColumnMenu = null;
					previewColumnMenu = seleniumReusableMethods.findElement(webDriver, previewMenuLocatorType,
							previewMenulocatorValue);
					new WebDriverWait(webDriver, 30).until(ExpectedConditions.elementToBeClickable(previewColumnMenu))
							.click();
					// System.out.println("********** remove column menu triggerd");

					String xpathDetailsOfRemoveButton = xpathDetails.getTitle();
					String[] splitRemoveColumn = xpathDetailsOfRemoveButton.split("&&");
					String removeButtonLocatorType = splitRemoveColumn[0];
					String removeButtonlocatorValue = splitRemoveColumn[1];

					WebElement removeElement = seleniumReusableMethods.findElement(webDriver, removeButtonLocatorType,
							removeButtonlocatorValue);

					if (removeElement != null) {
						// System.out.println("********** remove column ");
						removeElement.click();
					} else {
						throw new ThrowNewException("Unable to find Remove Column Button from current screen");
					}

					String xpathDetailsOfRefreshButton = xpathDetails.getTitle();
					String[] splitRefreshButton = xpathDetailsOfRefreshButton.split("&&");
					String refreshButtonLocatorType = splitRefreshButton[0];
					String refreshButtonlocatorValue = splitRefreshButton[1];
					WebElement refreshElement = seleniumReusableMethods.findElement(webDriver, refreshButtonLocatorType,
							refreshButtonlocatorValue);

					if (refreshElement != null) {
						// System.out.println("********** refreshElement clicked ");
						refreshElement.click();
					}

					listOfWebElement = seleniumReusableMethods.findElements(webDriver, previewMenuLocatorType,
							previewMenulocatorValue, 60, 1);

					numberOfColumns = listOfWebElement.size();
					// System.out.println("********** numberOfColumns " + numberOfColumns);
				}
				extendReportClass.reportPass(webDriver, extentTest, validationPoint,
						"All the colums are removed from the preview section.");
			} else {
				throw new ThrowNewException("There are no column present in preview section");
			}

		} catch (

		ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(
					"********** Exception during remove column functionality under report preview section " + e);
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception during remove column functionality under report preview section" + e.toString());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;
	}

	/**
	 * @author Wisefinch Menaka
	 * @see To work on date picket element
	 * 
	 * @param webDriver                  --> Current instance of {@link WebDriver}
	 * @param extentReport               --> Current instance of
	 *                                   {@link ExtentReports}
	 * @param extentTest                 --> Current instance of {@link ExtentTest}
	 * @param validationPoint            --> If yes screenshot will be attached ,
	 *                                   else only information will be added to
	 *                                   reportTest html file.
	 * @param elementLocatorTypeAndValue --> Pass locator type and value details
	 *                                   [This details should be shared from
	 *                                   xpathDetails HashMap]
	 * @param elementName                --> To select value from date picker, 4
	 *                                   element name should be passed here by
	 *                                   separated with comma (,). Following element
	 *                                   xpath should be identified and added.
	 *                                   1.Date input box element 2. month element
	 *                                   of type Select, 3. year element of type
	 *                                   Select, 4.Select date element. For date
	 *                                   element xpath, date value should be defined
	 *                                   as '%s'. Eg xpath :
	 *                                   .//*[@data-handler="selectDay"]/*[contains(text(),'%s')].
	 *                                   In date picker code date will be identified
	 *                                   from valueToEnter variable data and
	 *                                   replaced in the place of '%s'
	 * 
	 * @param valueToEnter               --> Value should be in the format of month
	 *                                   date year. We can use any special character
	 *                                   as a separator except Java special
	 *                                   characters.
	 * @return
	 * @throws ThrowNewException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	public boolean selectdatefromsalesforcedatepicker(WebDriver webDriver, ExtentReports extentReport,
			ExtentTest extentTest, String validationPoint, String elementName, String valueToEnter)
			throws ThrowNewException, IOException {

		Boolean methodExecuted = true;
		String[] requiredElements = elementName.split(",");

		String date = null, month = null, year = null;

		try {

			char getCharToSplit;

			if (valueToEnter != null) {
				int inputLength = valueToEnter.length();

				getCharToSplit = valueToEnter.charAt((inputLength - 5));

				String[] dateDetails = null;
				dateDetails = valueToEnter.split(Character.toString(getCharToSplit));

				month = dateDetails[0];
				date = dateDetails[1];

				if (date.charAt(0) == '0') {
					date = Character.toString(date.charAt(1));
				}
				year = dateDetails[2];

			} else {
				throw new ThrowNewException(
						"There are no input data provided. Input data expected in the order of Date Month Year this can be splitted with any Character Except Double ampersand '&&'");
			}

			if (month.equalsIgnoreCase("1") || month.equalsIgnoreCase("01") || month.equalsIgnoreCase("jan")
					|| month.equalsIgnoreCase("january")) {
				month = "January";
			} else if (month.equalsIgnoreCase("2") || month.equalsIgnoreCase("02") || month.equalsIgnoreCase("feb")
					|| month.equalsIgnoreCase("february")) {
				month = "February";
			} else if (month.equalsIgnoreCase("3") || month.equalsIgnoreCase("03") || month.equalsIgnoreCase("mar")
					|| month.equalsIgnoreCase("march")) {
				month = "March";
			} else if (month.equalsIgnoreCase("4") || month.equalsIgnoreCase("04") || month.equalsIgnoreCase("apr")
					|| month.equalsIgnoreCase("april")) {
				month = "April";
			} else if (month.equalsIgnoreCase("5") || month.equalsIgnoreCase("05") || month.equalsIgnoreCase("may")
					|| month.equalsIgnoreCase("may")) {
				month = "May";
			} else if (month.equalsIgnoreCase("6") || month.equalsIgnoreCase("06") || month.equalsIgnoreCase("jun")
					|| month.equalsIgnoreCase("june")) {
				month = "June";
			} else if (month.equalsIgnoreCase("7") || month.equalsIgnoreCase("07") || month.equalsIgnoreCase("jul")
					|| month.equalsIgnoreCase("july")) {
				month = "July";
			} else if (month.equalsIgnoreCase("8") || month.equalsIgnoreCase("08") || month.equalsIgnoreCase("aug")
					|| month.equalsIgnoreCase("aug")) {
				month = "August";
			} else if (month.equalsIgnoreCase("9") || month.equalsIgnoreCase("09") || month.equalsIgnoreCase("sep")
					|| month.equalsIgnoreCase("september")) {
				month = "September";
			} else if (month.equalsIgnoreCase("10") || month.equalsIgnoreCase("oct")
					|| month.equalsIgnoreCase("october")) {
				month = "October";
			} else if (month.equalsIgnoreCase("11") || month.equalsIgnoreCase("nov")
					|| month.equalsIgnoreCase("november")) {
				month = "November";
			} else if (month.equalsIgnoreCase("12") || month.equalsIgnoreCase("dec")
					|| month.equalsIgnoreCase("december")) {
				month = "December";
			} else {
				throw new ThrowNewException(
						"Provide valid month value. Eg  1 or Jan or January or 01. " + month + " is not a valid input");
			}

			String monthXpathAndDetails = xpathDetails.get(elementName);
			String[] splitGetMonthDetails = monthXpathAndDetails.split("&&");
			String monthXpathLocatorType = splitGetMonthDetails[0];
			String monthXpathLocatorValue = splitGetMonthDetails[1];
			WebElement calenderElement = seleniumReusableMethods.findElement(webDriver, monthXpathLocatorType,
					monthXpathLocatorValue);

			if (calenderElement != null) {
				calenderElement.click();
			} else {
				throw new ThrowNewException("Unable to locate " + elementName + " in current screen.");
			}

			String nextMonthButtonXpathAndDetails = xpathDetails.get("Opportunities_Next Month Button Of Calender");
			String[] splitNextMonthDetailsButton = nextMonthButtonXpathAndDetails.split("&&");
			String nextMonthButtonXpathLocatorType = splitNextMonthDetailsButton[0];
			String nextMonthButtonXpathLocatorValue = splitNextMonthDetailsButton[1];
			WebElement nextMonthButtonElement = seleniumReusableMethods.findElement(webDriver,
					nextMonthButtonXpathLocatorType, nextMonthButtonXpathLocatorValue);

			if (nextMonthButtonElement == null) {
				throw new ThrowNewException(
						"Unable to locate Next Month Button Of Calender element in current screen.");
			}

			String getMonthXpathAndDetails = xpathDetails.get("Opportunities_Get Month Of Calender");
			String[] splitGetMonthDetailsElement = getMonthXpathAndDetails.split("&&");
			String getMonthElementXpathLocatorType = splitGetMonthDetailsElement[0];
			String getMonthElementXpathLocatorValue = splitGetMonthDetailsElement[1];
			WebElement getMonthDetailsElement = seleniumReusableMethods.findElement(webDriver,
					getMonthElementXpathLocatorType, getMonthElementXpathLocatorValue);

			String monthTextFromCalender = null;
			if (getMonthDetailsElement == null) {
				throw new ThrowNewException("Unable to locate Get Month Of Calender element in current screen.");
			} else {

				monthTextFromCalender = seleniumReusableMethods.getTextForCode(webDriver, extentReport, extentTest,
						validationPoint, elementName, getMonthDetailsElement);

				while (!monthTextFromCalender.equalsIgnoreCase(month)) {
					nextMonthButtonElement.click();
					monthTextFromCalender = null;

					monthTextFromCalender = seleniumReusableMethods.getTextForCode(webDriver, extentReport, extentTest,
							validationPoint, elementName, getMonthDetailsElement);
					System.out.println("*********** month : " + month);
					System.out.println("*********** monthTextFromCalender : " + monthTextFromCalender);
				}
			}

			/*
			 * extendReportClass.reportPass(webDriver, extentTest, validationPoint,
			 * "Given month : " + month + " Selected Month " + monthTextFromCalender);
			 */

			String selectYearElementXpathAndDetails = xpathDetails.get("Opportunities_Select Year Of Calender");
			String[] selectYearElementLocatorTypeAndValue = selectYearElementXpathAndDetails.split("&&");
			String selectYearXpathLocatorType = selectYearElementLocatorTypeAndValue[0];
			String selectYearXpathLocatorValue = selectYearElementLocatorTypeAndValue[1];
			WebElement selectYearElement = seleniumReusableMethods.findElement(webDriver, selectYearXpathLocatorType,
					selectYearXpathLocatorValue);

			if (selectYearElement == null) {
				throw new ThrowNewException("Unable to locate Select Year Of Calender element in current screen.");
			} else {
				Select selectYear = new Select(selectYearElement);
				selectYear.selectByVisibleText(year);
			}

			String selectDateXpathAndDetails = xpathDetails.get("Opportunities_Select Date");
			String[] selectDateElementLocatorTypeAndValue = selectDateXpathAndDetails.split("&&");
			String selectDateXpathLocatorType = selectDateElementLocatorTypeAndValue[0];
			String selectDateXpathLocatorValue = selectDateElementLocatorTypeAndValue[1].replaceAll("%s", date);
			WebElement selectDateElement = seleniumReusableMethods.findElement(webDriver, selectDateXpathLocatorType,
					selectDateXpathLocatorValue);

			List<WebElement> selectDateElements = seleniumReusableMethods.findElements(webDriver,
					selectDateXpathLocatorType, selectDateXpathLocatorValue, 60, 1);

			if (selectDateElement == null) {
				throw new ThrowNewException("Unable to locate Select Date Of Calender element in current screen.");
			} else {
				int elementSize = selectDateElements.size();
				System.out.println("*********** selectDateElements size " + elementSize);
				if (selectDateElements.size() > 1) {
					if (Integer.parseInt(date) < 10) {
						selectDateElements.get(0).click();
					} else {
						selectDateElements.get(elementSize - 1).click();
					}
				} else {
					selectDateElement.click();
				}
			}

			extendReportClass.reportPass(webDriver, extentTest, validationPoint,
					valueToEnter + " is selected for the element " + elementName);

		} catch (

		ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;

		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during date picker select functionality " + e);
			extendReportClass.reportFail(webDriver, extentTest,
					"Unable to select date " + valueToEnter + " from datepicker" + e.toString());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To add Fields under report of sales force
	 * 
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file.
	 * @param testData        --> Test data to perform add fields. --> Expecting
	 *                        Field Name
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 */
	@SuppressWarnings("null")
	public Boolean addField(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String testData) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		String fieldName;

		if (testData != null) {
			try {
				fieldName = testData;
			} catch (Throwable e) {
				throw new ThrowNewException("The method addFilterForReport expecting field name as a data");
			}
		} else {
			throw new ThrowNewException("The method addFilterForReport expecting field name as a data");
		}

		try {

			String xpathDetailsOfTriggerFieldPanel = xpathDetails.get("Reports_FieldsPanel");
			String[] split = xpathDetailsOfTriggerFieldPanel.split("&&");
			String fieldPanelLocatorType = split[0];
			String fieldPanellocatorValue = split[1];
			WebElement clickFieldPanel = seleniumReusableMethods.findElement(webDriver, fieldPanelLocatorType,
					fieldPanellocatorValue);

			if (clickFieldPanel != null) {
				clickFieldPanel.click();
			} else {
				throw new ThrowNewException("Unable to find the field panel");
			}

			String xpathDetailsOfSearchFieldInputBox = xpathDetails.get("Reports_Search Fields Text Box");
			String[] searchFieldInputBoxsplit = xpathDetailsOfSearchFieldInputBox.split("&&");
			String searchFieldInputBoxLocatorType = searchFieldInputBoxsplit[0];
			String searchFieldInputBoxlocatorValue = searchFieldInputBoxsplit[1];

			WebElement searchFieldInputBox = seleniumReusableMethods.findElement(webDriver,
					searchFieldInputBoxLocatorType, searchFieldInputBoxlocatorValue);

			if (searchFieldInputBox != null) {
				System.out.println("********** " + searchFieldInputBox.isDisplayed());
				System.out.println("********** " + searchFieldInputBox.isEnabled());
				searchFieldInputBox.sendKeys(fieldName);

			} else {
				throw new ThrowNewException("Unable to find the search field input box");
			}

			String xpathDetailsOfSearchResult = xpathDetails.get("Reports_Double Click Search Result");
			String[] splitSearchResult = xpathDetailsOfSearchResult.split("&&");
			String searchResultLocatorType = splitSearchResult[0];
			String searcjResultlocatorValue = splitSearchResult[1];

			searcjResultlocatorValue = searcjResultlocatorValue.replace("%s", fieldName);
			System.out.println("********** selectFilterlocatorValue " + searcjResultlocatorValue);
			WebElement selectFilter = seleniumReusableMethods.findElement(webDriver, searchResultLocatorType,
					searcjResultlocatorValue);
			if (selectFilter != null) {

				Actions action = new Actions(webDriver);
				action.doubleClick(selectFilter).build().perform();

				// ((JavascriptExecutor)webDriver).executeScript("arguments[0].doubleClick();",
				// selectFilter);

				/*
				 * ((JavascriptExecutor)
				 * webDriver).executeScript("var evt = document.createEvent('MouseEvents');"+
				 * "evt.initMouseEvent('dblclick',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);"
				 * + "arguments[0].dispatchEvent(evt);", selectFilter);
				 */

				System.out.println("********** double click performed");
			} else {
				throw new ThrowNewException("Unable to find any result out of filter");
			}

			String hideFilterXpathDetails = xpathDetails.get("Reports_Hide Field");
			String[] hideFilterSplit = hideFilterXpathDetails.split("&&");
			String hideFilterLocatorType = hideFilterSplit[0];
			String hideFilterlocatorValue = hideFilterSplit[1];
			WebElement clickFilter = seleniumReusableMethods.findElement(webDriver, hideFilterLocatorType,
					hideFilterlocatorValue);
			if (clickFilter != null) {
				clickFilter.click();
			} else {
				throw new ThrowNewException("Unable to find hide filter button");
			}

			String xpathDetailsOfRefreshButton = xpathDetails.get("Reports_Refresh Button");
			String[] splitRefreshButton = xpathDetailsOfRefreshButton.split("&&");
			String refreshButtonLocatorType = splitRefreshButton[0];
			String refreshButtonlocatorValue = splitRefreshButton[1];
			WebElement refreshElement = seleniumReusableMethods.findElement(webDriver, refreshButtonLocatorType,
					refreshButtonlocatorValue);

			if (refreshElement != null) {
				// System.out.println("********** refreshElement clicked ");
				refreshElement.click();
			}

			String addFieldsXpathDetails = xpathDetails.get("Reports_Added Field Check");
			String[] addedFieldSplit = addFieldsXpathDetails.split("&&");
			String addedFieldLocatorType = addedFieldSplit[0];
			String addedFieldlocatorValue = addedFieldSplit[1];
			addedFieldlocatorValue = addedFieldlocatorValue.replaceAll("%s", fieldName);
			WebElement addedfiled = seleniumReusableMethods.findElement(webDriver, addedFieldLocatorType,
					addedFieldlocatorValue);
			List<WebElement> addedField = seleniumReusableMethods.findElements(webDriver, addedFieldLocatorType,
					addedFieldlocatorValue, 60, 1);

			if (addedField.size() > 0) {
				extendReportClass.reportPass(webDriver, extentTest, validationPoint,
						"Field " + fieldName + " added successfully");
			} else {
				extendReportClass.reportFail(webDriver, extentTest, "Field " + fieldName + " is not added");
			}

		} catch (ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during add field function " + e);
			extendReportClass.reportFail(webDriver, extentTest,
					"Unable to perform  add field function ." + e.toString());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To perform add filter under reports of sales force
	 * 
	 * @param webDriver       --> Current instance of {@link WebDriver}
	 * @param extentReport    --> Current instance of {@link ExtentReports}
	 * @param extentTest      --> Current instance of {@link ExtentTest}
	 * @param validationPoint --> If yes screenshot will be attached , else only
	 *                        information will be added to reportTest html file
	 * @param testData        --> Filter type, operator name and operator values are
	 *                        expected
	 * @return
	 * @throws IOException
	 * @throws ThrowNewException
	 */
	@SuppressWarnings("null")
	public Boolean addFilterForReport(WebDriver webDriver, ExtentReports extentReport, ExtentTest extentTest,
			String validationPoint, String testData) throws IOException, ThrowNewException {

		Boolean methodExecuted = true;
		String filterType, operatorName, operatorValue;

		if (testData != null) {
			try {
				String[] splitTestData = testData.split(",");
				filterType = splitTestData[0];
				operatorName = splitTestData[1];
				operatorValue = splitTestData[2];
			} catch (Throwable e) {
				throw new ThrowNewException(
						"The method addFilterForReport expecting following test data's. Filter type, operator name and it's value");
			}
		} else {
			throw new ThrowNewException(
					"The method addFilterForReport expecting following test data's. Filter type, operator name and it's value");
		}

		try {

			String xpathDetailsOfAddFilters = xpathDetails.get("Reports_Filters");
			String[] split = xpathDetailsOfAddFilters.split("&&");
			String filterLocatorType = split[0];
			String filterlocatorValue = split[1];
			WebElement clickFilters = seleniumReusableMethods.findElement(webDriver, filterLocatorType,
					filterlocatorValue);

			if (clickFilters != null) {
				clickFilters.click();
			} else {
				throw new ThrowNewException("Unable to find the filter link");
			}

			String xpathDetailsOfAddFilterInputBox = xpathDetails.get("Reports_Filter Input Box");
			String[] addFilterInputBoxsplit = xpathDetailsOfAddFilterInputBox.split("&&");
			String addFilterInputBoxLocatorType = addFilterInputBoxsplit[0];
			String addFilterInputBoxlocatorValue = addFilterInputBoxsplit[1];

			WebElement addFilterInputBox = seleniumReusableMethods.findElement(webDriver, addFilterInputBoxLocatorType,
					addFilterInputBoxlocatorValue);

			if (addFilterInputBox != null) {
				System.out.println("********** " + addFilterInputBox.isDisplayed());
				System.out.println("********** " + addFilterInputBox.isEnabled());
				addFilterInputBox.sendKeys(filterType);

			} else {
				throw new ThrowNewException("Unable to find the filter input box");
			}

			String xpathDetailsOfSelectFilter = xpathDetails.get("Reports_Select Filtered Value");
			String[] splitSelectFilter = xpathDetailsOfSelectFilter.split("&&");
			String selectFilterLocatorType = splitSelectFilter[0];
			String selectFilterlocatorValue = splitSelectFilter[1];

			selectFilterlocatorValue = selectFilterlocatorValue.replace("%s", filterType);
			System.out.println("********** selectFilterlocatorValue " + selectFilterlocatorValue);
			WebElement selectFilter = seleniumReusableMethods.findElement(webDriver, selectFilterLocatorType,
					selectFilterlocatorValue);
			if (selectFilter == null) {
				int j = 0;
				while (j < 4) {
					addFilterInputBox.click();
					addFilterInputBox.sendKeys(Keys.BACK_SPACE);
					addFilterInputBox.clear();
					addFilterInputBox.sendKeys(filterType);
					System.out.println("********** Cleared and entered new value");
					selectFilter = null;
					selectFilter = seleniumReusableMethods.findElement(webDriver, selectFilterLocatorType,
							selectFilterlocatorValue);
					if (selectFilter == null) {
						System.out.println("********** J value incremented " + j);
						j = j + 1;
					} else {
						selectFilter.click();
						break;
					}

				}

			} else if (selectFilter != null) {
				// Thread.sleep(5000);
				selectFilter.click();
				System.out.println("********** clciked on first try");
			} else {
				throw new ThrowNewException("Unable to find any result out of filter");
			}

			String clickFilterOperatorOfAddFilters = xpathDetails.get("Reports_Click Filter Operator");
			String[] clickFilterOperatorSplit = clickFilterOperatorOfAddFilters.split("&&");
			String clickFilterOperatorLocatorType = clickFilterOperatorSplit[0];
			String clickFilterOperatorlocatorValue = clickFilterOperatorSplit[1];
			WebElement clickFilter = seleniumReusableMethods.findElement(webDriver, clickFilterOperatorLocatorType,
					clickFilterOperatorlocatorValue);
			if (clickFilter != null) {
				clickFilter.click();
			} else {
				throw new ThrowNewException("Unable to find filter combo box");
			}

			if (!operatorName.equals("equals")) {
				String selectOperatorOfAddFilters = xpathDetails.get("Reports_Select Operator");
				String[] selectOperatorSplit = selectOperatorOfAddFilters.split("&&");
				String selectOperatorLocatorType = selectOperatorSplit[0];
				String selectOperatorLocatorValue = selectOperatorSplit[1];
				selectOperatorLocatorValue = selectOperatorLocatorValue.replace("%s", operatorName);
				WebElement selectOperator = seleniumReusableMethods.findElement(webDriver, selectOperatorLocatorType,
						selectOperatorLocatorValue);
				if (selectOperator != null) {
					selectOperator.click();
				} else {
					throw new ThrowNewException("Unable to find operator element");
				}
			}

			String operatorValueAddFilters = xpathDetails.get("Reports_Operator Value");
			String[] operatorValueSplit = operatorValueAddFilters.split("&&");
			String operatorValueLocatorType = operatorValueSplit[0];
			String operatorValueLocatorValue = operatorValueSplit[1];

			WebElement provideOperatorValue = seleniumReusableMethods.findElement(webDriver, operatorValueLocatorType,
					operatorValueLocatorValue);
			if (provideOperatorValue != null) {
				provideOperatorValue.sendKeys(operatorValue);
			} else {
				throw new ThrowNewException("Unable to find Operator value input box element");
			}

			String applyButtonAddFilters = xpathDetails.get("Reports_Apply Button");
			String[] applyButtonSplit = applyButtonAddFilters.split("&&");
			String applyButtonLocatorType = applyButtonSplit[0];
			String applyButtonLocatorValue = applyButtonSplit[1];

			WebElement applyButton = seleniumReusableMethods.findElement(webDriver, applyButtonLocatorType,
					applyButtonLocatorValue);
			if (applyButton != null) {
				applyButton.click();

			} else {
				throw new ThrowNewException("Unable to find applyButton element");
			}

			extendReportClass.reportPass(webDriver, extentTest, validationPoint,
					"Filter " + filterType + " added successfully");

		} catch (

		ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest, e.getErrorMessage());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("********** Exception during add filter function " + e);
			extendReportClass.reportFail(webDriver, extentTest,
					"Unable to perform  add filter function ." + e.toString());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, validationPoint);
			extendReportClass.endReport(extentReport, extentTest, validationPoint);
			methodExecuted = false;
		}
		return methodExecuted;

	}

}
