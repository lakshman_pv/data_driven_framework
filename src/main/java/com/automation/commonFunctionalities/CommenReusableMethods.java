package com.automation.commonFunctionalities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class CommenReusableMethods extends BaseClass {

	/**
	 * @author Wisefinch Menaka
	 * @see This method is to get current date and time based on argument passed
	 * 
	 * @param dateAndTimeFormet --> the format can be a combination of
	 *                          YYYY,MM,DD,MM,SS, HH [Ex - yyyyMMdd_HHmmss ]
	 * @return --> date and time based on format passed to dateAndTimeFormet
	 *         parameter
	 * @throws ThrowNewException
	 */
	public static String getCurrentDateAndTime(String dateAndTimeFormat) throws ThrowNewException {
		try {
			DateTimeFormatter dateTimeFormater = DateTimeFormatter.ofPattern(dateAndTimeFormat);
			LocalDateTime currentTime = LocalDateTime.now();
			// System.out.println(dateTimeFormater.format(currentTime));
			dateAndTimeFormat = dateTimeFormater.format(currentTime);
		} catch (Throwable e) {
			throw new ThrowNewException("Exception when trying to retrieve current date and time. " + e);
		}
		return dateAndTimeFormat;
	}

	/**
	 * @author Wisefinch Menaka
	 * @see to save runtime test data
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Boolean saveRunTimeTestData(String key, String value) {
		Boolean methodExecuted = true;

		try {
			if (key != null) {
				if (value != null) {
					runTimeTestDatas.put(key, value);
				} else {
					throw new ThrowNewException("There are no runtime save data provided");
				}
			} else {
				throw new ThrowNewException("There are no key value provided");
			}
		} catch (ThrowNewException e) {
			methodExecuted = false;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("********** Exception during click functionality " + e);
			methodExecuted = false;
		}
		return methodExecuted;

	}

	/**
	 * @author Wisefinch Menaka
	 * @see To read the property file
	 * @throws FileNotFoundException
	 */
	public void readPropertyFile() throws FileNotFoundException {
		try {
			System.out.println("********** workingDir " + workingDir);
			FileReader reader = new FileReader(workingDir + "\\src\\main\\resources\\application.properties");
			properties = new Properties();
			properties.load(reader);
			System.out.println(properties.getProperty("Salesforce"));
			System.out.println(properties.getProperty("Browser"));
		} catch (IOException e) {
			System.out.println("********** Exception During Property Read");
			e.printStackTrace();
		}
	}
}
