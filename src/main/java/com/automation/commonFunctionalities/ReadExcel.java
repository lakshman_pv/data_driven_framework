package com.automation.commonFunctionalities;

import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.io.FileOutputStream;

import java.io.IOException;

import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFCell;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.automation.letsMakeItSimplePOM.BaseClass;

public class ReadExcel extends BaseClass {

	private static XSSFSheet ExcelWSheet;

	private static XSSFWorkbook ExcelWBook;

	private static XSSFCell Cell;

	private static XSSFRow Row;

	public static Object[][] getTableArray(String FilePath, String SheetName) throws Exception {

		String[][] tabArray = null;

		try {

			FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			int startRow = 2;

			int startCol = 1;

			int ci, cj;

			int totalRows = ExcelWSheet.getLastRowNum();
			System.out.println("total rows : " + totalRows);

			int lastrow =totalRows-1;

			// you can write a function as well to get Column count
			Row = ExcelWSheet.getRow(startRow);

			int totalCols = Row.getLastCellNum();
			System.out.println("total columns : " + totalCols);

			// to avoid the Result column
			int dataColumn = totalCols - 2;

			tabArray = new String[lastrow][dataColumn];
			System.out.println("********** tabArray  length : " + tabArray.length);

			ci = 0;

			for (int i = startRow; i <= totalRows; i++, ci++) {

				cj = 0;

				for (int j = startCol; j <= dataColumn; j++, cj++) {

					tabArray[ci][cj] = getCellData(i, j);
					//System.out.println("tabArray[" + ci + "][" + cj + "]");
					System.out.println(tabArray[ci][cj]);

				}

			}

		}

		catch (FileNotFoundException e) {

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();

		}

		catch (IOException e) {

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();

		}

		return (tabArray);

	}

	public static String getCellData(int RowNum, int ColNum) throws Exception {

		try {

			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

			int dataType = Cell.getCellType();

			if (dataType == 3) {

				return "";

			} else {
				if (dataType == 1) {

					String CellData = Cell.getStringCellValue();

					return CellData;
				} else {

					int dats = (int) Cell.getNumericCellValue();

					String CellData = String.valueOf(dats);

					return CellData;
				}

			}
		} catch (Exception e) {

			System.out.println(e.getMessage());

			throw (e);

		}

	}

}
