package com.automation.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class Users_OrangeHRM extends BaseClass {

	private WebDriver webDriver;

	public Users_OrangeHRM(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);

	}

	@FindBy(xpath = ".//a[contains(text(),'Luke.Wright')]")
	public WebElement lukeWrightUser;

}
