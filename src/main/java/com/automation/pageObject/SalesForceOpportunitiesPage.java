package com.automation.pageObject;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

@Component
public class SalesForceOpportunitiesPage extends BaseClass {

	private WebDriver webDriver;
	ExtendReportClass extendReportClass = new ExtendReportClass();
	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();

	public SalesForceOpportunitiesPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	@FindBy(xpath = ".//a/*[contains(text(),'New')]")
	public WebElement newButton;

	@FindBy(xpath = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement dropDownIcon;

	@FindBy(xpath = "//a[@name='Edit']")
	public WebElement editOpportunity;

	@FindBy(xpath = "//input[@placeholder='Search this list...']")
	public WebElement searchField;

	@FindBy(xpath = "//table//tbody//tr[1]")
	public WebElement firstOppurtunity;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Name']")
	public WebElement opportunityName;

	@FindBy(xpath = "//span[@class='slds-checkbox_faux']")
	public WebElement isPrivate;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Amount']")
	public WebElement amount;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='CloseDate']")
	public WebElement closeDate;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='NextStep']")
	public WebElement nextStep;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[1]")
	public WebElement accountName;

	@FindBy(xpath = "//force-record-layout-row[4]//div[@class='slds-input__icon-group slds-input__icon-group_right']//button")
	public WebElement clearAccountSelection;

	@FindBy(xpath = "//force-record-layout-row[7]//div[@class='slds-input__icon-group slds-input__icon-group_right']//button")
	public WebElement clearCampaignSourceSelection;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[2]")
	public WebElement stage;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[3]")
	public WebElement type;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='Probability']")
	public WebElement probability;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[4]")
	public WebElement leadSource;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[5]")
	public WebElement primeryCampaignSource;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='OrderNumber__c']")
	public WebElement orderNumber;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='MainCompetitors__c']")
	public WebElement mainCompetitors;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='CurrentGenerators__c']")
	public WebElement currentGenerators;

	@FindBy(xpath = "(.//*[@class='actionBody']//*[@role='combobox'])[6]")
	public WebElement delveryInstallatonStatus;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='TrackingNumber__c']")
	public WebElement trackingNumber;

	@FindBy(xpath = ".//*[@class='actionBody']//force-record-layout-section[3]//textarea")
	public WebElement description;

	@FindBy(xpath = ".//*[@class='actionBody']//*[@name='SaveEdit']")
	public WebElement save;

	public String searchOpportunityTable = "//tbody/tr/th//a[@title='%s']";

	public String selectValueFromDropDown = ".//*[@class='actionBody']//*[contains(text(),'%s')]";

	public String selectLightningFormatText = "//lightning-base-combobox-formatted-text[@title='%s']";

	public String newOpportunityCheck = ".//*[@class='slds-grid slds-wrap']//*[contains(text(),'%s')]";

	public boolean searchOpportunity(ExtentReports extentReport, ExtentTest extentTest, String OpportunityName)
			throws IOException, ThrowNewException {
		boolean stat = true;
		try {
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "Oppurtunity Search", searchField,
					"isDisplayed");
			seleniumReusableMethods.sendKeyAndEnter(webDriver, extentReport, extentTest, "Y", "Oppurtunity Search",
					searchField, OpportunityName);
			String searchAccount = searchOpportunityTable.replaceAll("%s", OpportunityName);
			System.out.println("xpath : " + searchAccount);
			seleniumReusableMethods.verifyElement(webDriver, extentReport, extentTest, "Y", "searchAccount",
					searchAccount, "isDisplayed");

			if (methodexecutionStat == true) {
				System.out.println("Oppurtunity existing");
			} else {
				System.out.println("Oppurtunity to be created");
				stat = false;
			}

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform oppurtunity search. " + e);
			e.printStackTrace();
			return stat = false;
		}
		return stat;

	}

	public boolean createOpportunities(ExtentReports extentReport, ExtentTest extentTest, String OpportunityName,
			String Amount, String CloseDate, String NextStep, String AccountName, String Stage, String Type,
			String Probability, String LeadSource, String PrimeryCampaignSource, String OrderNumber,
			String MainCompetitors, String CurrentGenerators, String DeliveryInstallationStatus, String TrackingNumber,
			String Description) throws IOException, ThrowNewException, InterruptedException {
		Boolean stat = true;
		try {

			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "New Button", newButton,
					"isDisplayed");
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "New Button", newButton);

			seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Oppurtunity Name",
					opportunityName, OpportunityName);

			if (Amount == null || Amount == "" || Amount == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Amount", amount, Amount);
			}

			if (CloseDate == null || CloseDate == "" || CloseDate == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Close Date", closeDate,
						CloseDate);
			}

			if (NextStep == null || NextStep == "" || NextStep == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Next Step", nextStep,
						NextStep);
			}

			if (AccountName == null || AccountName == "" || AccountName == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Account Name", accountName,
						AccountName);
				String selectAccount = selectLightningFormatText.replaceAll("%s", AccountName);
				WebElement selectAccountElement = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectAccount);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Account",
						selectAccountElement);
			}

			if (Stage == null || Stage == "" || Stage == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Stage", stage);
				String selectStage = selectValueFromDropDown.replaceAll("%s", Stage);
				WebElement selectStageElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectStage);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Stage",
						selectStageElement);
			}

			if (Type == null || Type == "" || Type == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Type", type);
				String selectType = selectValueFromDropDown.replaceAll("%s", Type);
				WebElement selectTypeElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Type",
						selectTypeElement);
			}

			if (Probability == null || Probability == "" || Probability == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Probability", probability,
						Probability);
			}

			if (LeadSource == null || LeadSource == "" || LeadSource == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "LeadSource",
						leadSource);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "LeadSource", leadSource);
				String selectLeadSource = selectValueFromDropDown.replaceAll("%s", LeadSource);
				WebElement selectLeadSourceElement = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectLeadSource);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select LeadSource",
						selectLeadSourceElement);
			}

			if (PrimeryCampaignSource == null || PrimeryCampaignSource == "" || PrimeryCampaignSource == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Primery Campaign Source", primeryCampaignSource);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Primery Campaign Source",
						primeryCampaignSource, PrimeryCampaignSource);
				String selectXpath = selectLightningFormatText.replaceAll("%s", PrimeryCampaignSource);
				WebElement selectElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectXpath);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, Description,
						"select PrimeryCampaignSource", selectElement);
			}

			if (OrderNumber == null || OrderNumber == "" || OrderNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Order Number",
						orderNumber);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Order Number", orderNumber,
						OrderNumber);
			}

			if (MainCompetitors == null || MainCompetitors == "" || MainCompetitors == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Main Competitors",
						mainCompetitors);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Main Competitors",
						mainCompetitors, MainCompetitors);
			}

			if (CurrentGenerators == null || CurrentGenerators == "" || CurrentGenerators == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Current Generators", currentGenerators);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Current Generators",
						currentGenerators, CurrentGenerators);
			}

			if (DeliveryInstallationStatus == null || DeliveryInstallationStatus == ""
					|| DeliveryInstallationStatus == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Delivery/Installation Status", delveryInstallatonStatus);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Delivery/Installation Status",
						delveryInstallatonStatus);
				String selectXpath = selectValueFromDropDown.replaceAll("%s", DeliveryInstallationStatus);
				WebElement selectElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectXpath);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y",
						"select Delivery/Installation Status", selectElement);
			}

			if (TrackingNumber == null || TrackingNumber == "" || TrackingNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Tracking Number",
						trackingNumber);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Tracking Number",
						trackingNumber, TrackingNumber);
			}

			if (Description == null || Description == "" || Description == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Description",
						description);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Description", description,
						Description);
			}

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Save", save);

			String checkOppurtunity = newOpportunityCheck.replaceAll("%s", OpportunityName);
			WebElement checkCreatedOppurtunity = seleniumReusableMethods.findElement(webDriver, "xpath",
					checkOppurtunity);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "check New Oppurtunity",
					checkCreatedOppurtunity, "isDisplayed");

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform oppurtunity search. " + e);
			stat = false;
			e.printStackTrace();
		}

		return stat;
	}

	public boolean editOppurtunities(ExtentReports extentReport, ExtentTest extentTest, String OpportunityName,
			String Amount, String CloseDate, String NextStep, String AccountName, String Stage, String Type,
			String Probability, String LeadSource, String PrimeryCampaignSource, String OrderNumber,
			String MainCompetitors, String CurrentGenerators, String DeliveryInstallationStatus, String TrackingNumber,
			String Description) throws IOException, ThrowNewException, InterruptedException {
		Boolean stat = true;
		try {

			seleniumReusableMethods.refresh(webDriver, extentReport, extentTest, "N");
			String searchOpportunity = searchOpportunityTable.replaceAll("%s", OpportunityName);
			WebElement clickOpportunity = seleniumReusableMethods.findElement(webDriver, "xpath", searchOpportunity);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "click Opportunity",
					clickOpportunity);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "drop down trigger", dropDownIcon);
			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Edit Opportunity",
					editOpportunity);

			if (Amount == null || Amount == "" || Amount == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Amount", amount, Amount);
			}

			if (CloseDate == null || CloseDate == "" || CloseDate == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Close Date", closeDate,
						CloseDate);
			}

			if (NextStep == null || NextStep == "" || NextStep == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Next Step", nextStep,
						NextStep);
			}

			if (AccountName == null || AccountName == "" || AccountName == " ") {

				System.out.println("error value");
			} else {

				if (seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "N", "clear Account selection",
						clearAccountSelection, "isDisplayed")) {

					seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "clear Account selection",
							clearAccountSelection);
				}

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Account Name", accountName,
						AccountName);
				String selectAccount = selectLightningFormatText.replaceAll("%s", AccountName);
				WebElement selectAccountElement = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectAccount);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Account",
						selectAccountElement);
			}

			if (Stage == null || Stage == "" || Stage == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Stage", type);
				String selectStage = selectValueFromDropDown.replaceAll("%s", Stage);
				WebElement selectStageElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectStage);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Stage",
						selectStageElement);
			}

			if (Type == null || Type == "" || Type == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Type", stage);
				String selectType = selectValueFromDropDown.replaceAll("%s", Type);
				WebElement selectTypeElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectType);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select Type",
						selectTypeElement);
			}

			if (Probability == null || Probability == "" || Probability == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Probability", probability,
						Probability);
			}

			if (LeadSource == null || LeadSource == "" || LeadSource == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "LeadSource",
						leadSource);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "LeadSource", leadSource);
				String selectLeadSource = selectValueFromDropDown.replaceAll("%s", LeadSource);
				WebElement selectLeadSourceElement = seleniumReusableMethods.findElement(webDriver, "xpath",
						selectLeadSource);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "select LeadSource",
						selectLeadSourceElement);
			}

			if (PrimeryCampaignSource == null || PrimeryCampaignSource == "" || PrimeryCampaignSource == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Primery Campaign Source", primeryCampaignSource);
				if (seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "N",
						"clear Primery Campaign Source selection", clearCampaignSourceSelection, "isDisplayed")) {

					seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N",
							"clear Primery Campaign Source selection", clearCampaignSourceSelection);
				}
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "N", "Primery Campaign Source",
						primeryCampaignSource, PrimeryCampaignSource);
				String selectXpath = selectLightningFormatText.replaceAll("%s", PrimeryCampaignSource);
				WebElement selectElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectXpath);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, Description,
						"select PrimeryCampaignSource", selectElement);
			}

			if (OrderNumber == null || OrderNumber == "" || OrderNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Order Number",
						orderNumber);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Order Number", orderNumber,
						OrderNumber);
			}

			if (MainCompetitors == null || MainCompetitors == "" || MainCompetitors == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Main Competitors",
						mainCompetitors);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Main Competitors",
						mainCompetitors, MainCompetitors);
			}

			if (CurrentGenerators == null || CurrentGenerators == "" || CurrentGenerators == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Current Generators", currentGenerators);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Current Generators",
						currentGenerators, CurrentGenerators);
			}

			if (DeliveryInstallationStatus == null || DeliveryInstallationStatus == ""
					|| DeliveryInstallationStatus == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N",
						"Delivery/Installation Status", delveryInstallatonStatus);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "N", "Delivery/Installation Status",
						delveryInstallatonStatus);
				String selectXpath = selectValueFromDropDown.replaceAll("%s", DeliveryInstallationStatus);
				WebElement selectElement = seleniumReusableMethods.findElement(webDriver, "xpath", selectXpath);
				seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y",
						"select Delivery/Installation Status", selectElement);
			}

			if (TrackingNumber == null || TrackingNumber == "" || TrackingNumber == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Tracking Number",
						trackingNumber);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Tracking Number",
						trackingNumber, TrackingNumber);
			}

			if (Description == null || Description == "" || Description == " ") {

				System.out.println("error value");
			} else {

				seleniumReusableMethods.scrollIntoElement(webDriver, extentReport, extentTest, "N", "Description",
						description);
				seleniumReusableMethods.sendKey(webDriver, extentReport, extentTest, "Y", "Description", description,
						Description);
			}

			seleniumReusableMethods.click(webDriver, extentReport, extentTest, "Y", "Save", save);

			String checkOppurtunity = newOpportunityCheck.replaceAll("%s", OpportunityName);
			WebElement checkCreatedOppurtunity = seleniumReusableMethods.findElement(webDriver, "xpath",
					checkOppurtunity);
			seleniumReusableMethods.verify(webDriver, extentReport, extentTest, "Y", "check New Oppurtunity",
					checkCreatedOppurtunity, "isDisplayed");

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform oppurtunity search. " + e);
			stat = false;
			e.printStackTrace();
		}

		return stat;
	}
}
