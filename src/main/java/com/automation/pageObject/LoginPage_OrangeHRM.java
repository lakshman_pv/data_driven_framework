package com.automation.pageObject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class LoginPage_OrangeHRM extends BaseClass {

	private WebDriver webDriver;

	public LoginPage_OrangeHRM(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);

	}

	@FindBy(xpath = ".//*[@name='txtUsername']")
	public WebElement userNameField;

	@FindBy(xpath = ".//*[@name='txtPassword']")
	public WebElement passwordField;

	@FindBy(xpath = ".//*[@name='Submit']")
	public WebElement login;

	@FindBy(xpath = ".//*[@name='Submit']")
	List<WebElement> listOfElements;


}
