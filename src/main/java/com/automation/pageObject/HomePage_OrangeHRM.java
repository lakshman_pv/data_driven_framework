package com.automation.pageObject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.automation.letsMakeItSimplePOM.BaseClass;

@Component
public class HomePage_OrangeHRM extends BaseClass {

	private WebDriver webDriver;

	public HomePage_OrangeHRM(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);

	}

	@FindBy(xpath = ".//img[@alt='OrangeHRM']")
	public WebElement orangeHRMHomePageImage;

	@FindBy(xpath = ".//*[@id='menu_leave_viewLeaveModule']/*[contains(text(),'Leave')]")
	public WebElement leaveMainMenu;

	@FindBy(xpath = ".//*[@id='menu_admin_viewAdminModule']/*[contains(text(),'Admin')]")
	public WebElement adminMainMenu;

	@FindBy(xpath = ".//a[normalize-space()='Entitlements']")
	public WebElement entitlementsSubMenu;

	@FindBy(xpath = ".//a[normalize-space()='User Management']")
	public WebElement userManagement;

	@FindBy(xpath = ".//a[normalize-space()='Users']")
	public WebElement users;

	@FindBy(xpath = ".//*[@id='menu_leave_addLeaveEntitlement' and contains(text(),'Add Entitlements')]")
	public WebElement addEntitlements;
}
