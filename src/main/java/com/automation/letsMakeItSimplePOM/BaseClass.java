package com.automation.letsMakeItSimplePOM;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.ReadExcel;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.letsMakeItSimplePOM.BaseClass.SalesForceOppurtunitiesPage;
import com.automation.pageObject.SalesForceAccountsPage;
import com.automation.pageObject.SalesForceLoginPage;
import com.automation.pageObject.SalesForceOpportunitiesPage;
import com.automation.pageObject.SalesforceHomepage;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import java.net.URL;
import java.util.HashMap;

public abstract class BaseClass {

	public class SalesForceOppurtunitiesPage {

	}
	// For Appium URL setup
	public URL url;
	 public static boolean methodexecutionStat;
	public static String workingDir = System.getProperty("user.dir");
	// To Store All Module Name and it's Value
	protected static HashMap<String, String> moduleNameList = new HashMap<>();
	// To Store All xpath details and it's Value
	protected static HashMap<String, String> xpathDetails = new HashMap<>();
	// To Store All run time test data details
	protected static HashMap<String, String> runTimeTestDatas = new HashMap<>();
	// To Store list of elements
	protected static List<WebElement> listOfWebElement;
	public static String testCaseNumber;

	protected static String pathToStoreFiles = null;

	protected static Properties properties;

	protected static WebDriver webDriver;
	protected static ExtentReports extentReport;
	protected static ExtentTest extentTest;
	protected static ExtendReportClass extentReportClass;

	protected static SalesForceLoginPage salesForceLoginPage;
	protected static SalesforceHomepage salesforceHomepage;
	protected static SalesForceAccountsPage SalesForceAccountsPage;
	protected static SalesForceOpportunitiesPage salesForceOpportunitiesPage;
	protected static ReadExcel readExcel;

}
