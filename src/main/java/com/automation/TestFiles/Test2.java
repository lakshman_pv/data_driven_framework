package com.automation.TestFiles;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import com.automation.commonFunctionalities.CommenReusableMethods;
import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.ReadExcel;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.automation.letsMakeItSimplePOM.InitPage;
import com.relevantcodes.extentreports.ExtentTest;

public class Test2 extends BaseClass {

	public String launchURL, browserToLaunch;
	public Object[][] testObjArray;
	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();
	ExtendReportClass extendReportClass = new ExtendReportClass();
	CommenReusableMethods commonReusableMethods = new CommenReusableMethods();
	ExtendReportClass extentReportClass = new ExtendReportClass();

	@BeforeSuite
	public void before_all() throws ThrowNewException, FileNotFoundException {
		System.out.println("********** Before Suit called");
		commonReusableMethods.readPropertyFile();

		launchURL = properties.getProperty("Salesforce");
		browserToLaunch = properties.getProperty("Browser");

		extentReport = extentReportClass
				.initExtentReport("ExecutionReport" + commonReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss"));
	}

	@BeforeMethod
	public void initReport() throws ThrowNewException {

		System.out.println("********** Before test");
		webDriver = seleniumReusableMethods.initWebDriver(browserToLaunch, extentReport, extentTest, "Y");
		InitPage initPage = new InitPage(webDriver);
		System.out.println("********** Webdriver launched");

	}

	@AfterMethod
	public void closeBrowser() {
		webDriver.quit();
	}

	@AfterSuite
	public void closeExtendReport() {

		extentReportClass.endReport(extentReport, extentTest, pathToStoreFiles);
		System.out.println("********** After test");
	}

	@DataProvider
	public Object[][] ExcelRead() throws Exception {

		try {
			System.out.println("********** Data provider excell reader");

			testObjArray = ReadExcel.getTableArray(workingDir + "/src/main/resources/DataDrivenTesting.xlsx",
					"Opportunity");
			return testObjArray;
		} catch (Exception e) {
			System.out.println("********** Exception catured during data collection ");
		}
		return testObjArray;
	}

	@Test(dataProvider = "ExcelRead")
	public void excelReadTest(String OpportunityName, String Amount, String CloseDate, String NextStep,
			String AccountName, String Stage, String Type, String Probability, String LeadSource,
			String PrimeryCampaignSource, String OrderNumber, String MainCompetitors, String CurrentGenerators,
			String DeliveryInstallationStatus, String TrackingNumber, String Description)
			throws IOException, ThrowNewException, InterruptedException {
		System.out.println("********** Executing test");
		try {
			
			if (OpportunityName == null) {

				System.out.println("null value");

			} else {

				extentTest = extentReportClass.initExtentTest(extentReport, "Create New Account");
				extentReportClass.reportInfo(webDriver, extentTest, "Test Case Name : Create New Account And Verify.");
				seleniumReusableMethods.launchWebDriver(webDriver, extentReport, extentTest, "Y", launchURL);
				salesForceLoginPage.loginToSalseForceWebPage(extentReport, extentTest, "Y");
				salesforceHomepage.launchSalesForceApp(extentReport, extentTest, "Y", "Opportunities");
				System.out.println("opportunity read from excel " + OpportunityName);

				boolean verifyAccount = salesForceOpportunitiesPage.searchOpportunity(extentReport, extentTest,
						OpportunityName);

				if (verifyAccount == true) {

					System.out.println("Opportunity to be edited : " + OpportunityName);
					boolean stat = salesForceOpportunitiesPage.editOppurtunities(extentReport, extentTest,
							OpportunityName, Amount, CloseDate, NextStep, AccountName, Stage, Type,
							Probability, LeadSource, PrimeryCampaignSource, OrderNumber, MainCompetitors,
							CurrentGenerators, DeliveryInstallationStatus, TrackingNumber, Description);
					if (stat) {
						System.out.println("Opportunity edited");
					} else {
						System.out.println("Error in account edit method");
					}

				} else {

					System.out.println("Opportunity to be created : " + OpportunityName);
					boolean stat = salesForceOpportunitiesPage.createOpportunities(extentReport, extentTest,
							OpportunityName, Amount, CloseDate, NextStep, AccountName, Stage, Type,
							Probability, LeadSource, PrimeryCampaignSource, OrderNumber, MainCompetitors,
							CurrentGenerators, DeliveryInstallationStatus, TrackingNumber, Description);

					if (stat) {
						System.out.println("Opportunity created");
					} else {
						System.out.println("Error in account creation");
					}
				}

			}

		} catch (NullPointerException nullException) {
			System.out.println("***********nullException  " + nullException.toString());
			extentReportClass.reportFail(webDriver, extentTest, nullException.toString());
			seleniumReusableMethods.quit(webDriver, extentReport, extentTest, "Y");
			extentReportClass.endReport(extentReport, extentTest, "Y");

		} 
			  catch (IOException | ThrowNewException e) {
			  extendReportClass.reportFail(webDriver, extentTest,
			  "Exception when trying to perform account unit test. " + e);
			  e.printStackTrace(); }
			 
	}
}
