package com.automation.TestFiles;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import com.automation.commonFunctionalities.CommenReusableMethods;
import com.automation.commonFunctionalities.ExtendReportClass;
import com.automation.commonFunctionalities.ReadExcel;
import com.automation.commonFunctionalities.SeleniumReusableMethods;
import com.automation.commonFunctionalities.ThrowNewException;
import com.automation.letsMakeItSimplePOM.BaseClass;
import com.automation.letsMakeItSimplePOM.InitPage;
import com.relevantcodes.extentreports.ExtentTest;

public class Test1 extends BaseClass {

	public String launchURL, browserToLaunch;
	public Object[][] testObjArray;
	SeleniumReusableMethods seleniumReusableMethods = new SeleniumReusableMethods();
	ExtendReportClass extendReportClass = new ExtendReportClass();
	CommenReusableMethods commonReusableMethods = new CommenReusableMethods();
	ExtendReportClass extentReportClass = new ExtendReportClass();
	
	
	@BeforeSuite
	public void before_all() throws ThrowNewException, FileNotFoundException {
		System.out.println("********** Before Suit called");
		commonReusableMethods.readPropertyFile();

		launchURL = properties.getProperty("Salesforce");
		browserToLaunch = properties.getProperty("Browser");

		extentReport = extentReportClass
				.initExtentReport("ExecutionReport" + commonReusableMethods.getCurrentDateAndTime("yyyyMMdd_HHmmss"));
	}

	@BeforeMethod
	public void initReport() throws ThrowNewException {

		System.out.println("********** Before test");
		webDriver = seleniumReusableMethods.initWebDriver(browserToLaunch, extentReport, extentTest, "Y");
		InitPage initPage = new InitPage(webDriver);
		System.out.println("********** Webdriver launched");
		
	}

	@AfterMethod
	public void closeBrowser() {
		webDriver.quit();
	}

	@AfterSuite
	public void closeExtendReport() {
		
		extentReportClass.endReport(extentReport, extentTest, pathToStoreFiles);
		System.out.println("********** After test");
	}

	@DataProvider
	public Object[][] ExcelRead() throws Exception {

		testObjArray = ReadExcel.getTableArray(workingDir + "/src/main/resources/DataDrivenTesting.xlsx",
				"Account");
		return testObjArray;
	}
	
	@Test(dataProvider = "ExcelRead")
	public void excelReadTest(String AccountName, String Rating, String Phone, String Parent, String Fax,
			String AccountNumber, String Website, String AccountSite, String TickerSymbol, String Type,
			String Ownership, String Industry, String Employees, String AnnualRevenue, String SICcode,
			String BillingStreet, String BillingCity, String ShippingStreet, String BillingSP, String ShippingCity,
			String ShippingSP, String BillingZIP, String BillingCountry, String ShippingZIP, String ShippingCountry,
			String CustomerPriority, String SLA, String SLAExpirationDate, String SLASerialNo, String NoOfLocations,
			String UpsellOppurtunity, String Active, String Description)
			throws IOException, ThrowNewException, InterruptedException {

		try {
			if (AccountName == null) {
				
				System.out.println("null value");
				
			} else {

				extentTest = extentReportClass.initExtentTest(extentReport, "Create New Account");
				extentReportClass.reportInfo(webDriver, extentTest, "Test Case Name : Create New Account And Verify.");
				seleniumReusableMethods.launchWebDriver(webDriver, extentReport, extentTest, "Y", launchURL);
				salesForceLoginPage.loginToSalseForceWebPage(extentReport, extentTest, "Y");
				salesforceHomepage.launchSalesForceApp(extentReport, extentTest, "Y", "Accounts");
				System.out.println("account read from excel " + AccountName);

				boolean verifyAccount = SalesForceAccountsPage.searchAccount(extentReport, extentTest, AccountName);
				
				if (verifyAccount == true) {

					System.out.println("Account to be edited : " + AccountName);
					boolean stat = SalesForceAccountsPage.editAccount(extentReport, extentTest, AccountName, Rating, Phone, Parent,
							Fax, AccountNumber, Website, AccountSite, TickerSymbol, Type, Ownership, Industry,
							Employees, AnnualRevenue, SICcode, BillingStreet, BillingCity, ShippingStreet, BillingSP,
							ShippingCity, ShippingSP, BillingZIP, BillingCountry, ShippingZIP, ShippingCountry,
							CustomerPriority, SLA, SLAExpirationDate, SLASerialNo, NoOfLocations, UpsellOppurtunity,
							Active, Description);
					if(stat) {
						System.out.println("Account edited");
					} else {
						System.out.println("Error in account edit method");
					}

				} else {

					System.out.println("Account to be created : " + AccountName);
					boolean stat = SalesForceAccountsPage.createAccount(extentReport, extentTest, AccountName, Rating, Phone, Parent,
							Fax, AccountNumber, Website, AccountSite, TickerSymbol, Type, Ownership, Industry,
							Employees, AnnualRevenue, SICcode, BillingStreet, BillingCity, ShippingStreet, BillingSP,
							ShippingCity, ShippingSP, BillingZIP, BillingCountry, ShippingZIP, ShippingCountry,
							CustomerPriority, SLA, SLAExpirationDate, SLASerialNo, NoOfLocations, UpsellOppurtunity,
							Active, Description);
					
					if(stat) {
						System.out.println("Account created");
					} else {
						System.out.println("Error in account creation");
					}
				}
			
			}

		} catch (IOException | ThrowNewException e) {
			extendReportClass.reportFail(webDriver, extentTest,
					"Exception when trying to perform account unit test. " + e);
			e.printStackTrace();
		}
	}
}
